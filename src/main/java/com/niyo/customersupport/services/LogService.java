package com.niyo.customersupport.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.niyo.customersupport.pojos.LogPojo;
import com.niyo.customersupport.repositories.LogRepository;

@Service
public class LogService {
	
	@Autowired
	private LogRepository logRepository;

	public void saveLog(String operationName, String msg, String operationStatus) {
		LogPojo logPojo = new LogPojo();
		logPojo.setOperationName(operationName);
		logPojo.setMsg(msg);
		logPojo.setOperationStatus(operationStatus);
		logRepository.save(logPojo);
	}
}
