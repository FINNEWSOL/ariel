package com.niyo.customersupport.services;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.adapter.AttachmentAdapter;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.pojos.AttachmentPojo;
import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.FreshDeskConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.repositories.ConversationRepository;
import com.niyo.customersupport.repositories.TicketRepository;
import com.niyo.customersupport.utils.FileUtils;
import com.niyo.customersupport.utils.StringUtils;
import com.niyo.customersupport.utils.restutils.exceptions.ReplyAlreadyExistsException;
import com.niyo.customersupport.utils.restutils.exceptions.S3DownloadException;
import com.niyo.customersupport.utils.restutils.exceptions.TicketNotFoundException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConversationService {
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private ConversationRepository conversationRepository;
	
	@Autowired
	private AttachmentAdapter attachmentAdapter;
	
	@Autowired
	private StringUtils stringUtils;
	
	@Autowired
	private AgentService agentService;
	
	public ConversationPojo replyToTicket(Integer freshDeskTicketId) throws UnirestException, JSONException, IOException, TicketNotFoundException, ReplyAlreadyExistsException {
		TicketPojo ticketPojo = ticketRepository.findFirstByFreshdeskTicketId(freshDeskTicketId);
		if (ticketPojo == null) {
			throw new TicketNotFoundException();
		}
		org.json.JSONObject lastReply = getLastReply(freshDeskTicketId);
		long freshDeskConversationId = lastReply.getLong("id");
		
		ConversationPojo conversationPojo = new ConversationPojo();
		if (freshDeskConversationId != 0) {
			ConversationPojo conversationPojo2 = conversationRepository.findByFreshDeskConversationId(freshDeskConversationId);
			if (conversationPojo2 != null) {
				throw new ReplyAlreadyExistsException();
			}
			conversationPojo.setFreshDeskConversationId(freshDeskConversationId);
		}
		Long repliersFreshDeskId = lastReply.getLong("user_id");
		conversationPojo.setRepliersFreshDeskId(repliersFreshDeskId);
		conversationPojo.setTicketId(ticketPojo);
		String body = lastReply.getString("body_text");
		String wellFormattedBody = stringUtils.removeQuotedText(body);
		conversationPojo.setBody(wellFormattedBody);
		boolean isIncoming = lastReply.getBoolean("incoming");
		conversationPojo.setAgent(!isIncoming);
		boolean isPrivate = lastReply.getBoolean("private");
		conversationPojo.setPrivateNote(isPrivate);
		if (conversationPojo.isAgent()) {
			String name = agentService.getAgentNameById(repliersFreshDeskId);
			if (name != null) {
				conversationPojo.setRepliersName(name);
			}
		}
		org.json.JSONArray attachmentArray = lastReply.getJSONArray("attachments");
		int attachmentLength = attachmentArray.length();
		if (attachmentLength == 0) {
			return conversationRepository.save(conversationPojo);
		} else {
			List<File> files = downloadAttachmentFromFreshDesk(attachmentArray);
			List<AttachmentPojo> attachmentPojos = attachmentAdapter.getAttachmentPojoFromFiles(files, ticketPojo, conversationPojo);
			FileUtils.deleteFiles(files);
			conversationPojo.setAttachments(attachmentPojos);
		}
		if (ticketPojo.getStatus() == 4 || ticketPojo.getStatus() == 5) {
			ticketPojo.setStatus(2);
			ticketRepository.save(ticketPojo);
		}
		return conversationRepository.save(conversationPojo);
	}
	
	public org.json.JSONObject getLastReply(Integer freshDeskTicketId) throws UnirestException, JSONException {
		int size = 0;
		org.json.JSONObject lastReply = null;
		int pageNumber = 0;
		while(size % 30 == 0) {
			pageNumber++;
			JsonNode response = freshDeskClient.getConversationsOfTicket(freshDeskTicketId, pageNumber);
			org.json.JSONArray responseArray = response.getArray();
			size = responseArray.length();
			if (size == 0) {
				break;
			}
			lastReply = (org.json.JSONObject) responseArray.get(size - 1);
		}
		return lastReply;
	}
	
	public List<File> downloadAttachmentFromFreshDesk(org.json.JSONArray attachmentArray) throws UnirestException, IOException, JSONException {
		List<File> files = new ArrayList<>();
		int numberOfAttachments = attachmentArray.length();
		for (int i =0; i < numberOfAttachments ; i++) {
			org.json.JSONObject attachment = attachmentArray.getJSONObject(i);
			String url = (String) attachment.get("attachment_url");
			String fileName = (String) attachment.get("name");
			files.add(freshDeskClient.downloadAttachmentFromFreshDesk(url, fileName));
		}
		return files;
	}
	
	public ConversationPojo replyToTicket(long ticketId, ConversationPojo conversationPojo) throws UnirestException, JsonParseException, JsonMappingException, IOException {
		String body = conversationPojo.getBody();
		if (body == null) {
			conversationPojo.setBody("");
		}
		TicketPojo ticketPojo = ticketRepository.findFirstByTicketId(ticketId);
		Integer freshDeskTicketId = ticketPojo.getFreshdeskTicketId();
		conversationPojo.setRepliersFreshDeskId(ticketPojo.getFreshDeskRequesterId());
		List<AttachmentPojo> attachments = conversationPojo.getAttachments();
		FreshDeskConversationPojo freshDeskConversationPojo = dozerBeanMapper.map(conversationPojo, FreshDeskConversationPojo.class);
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> map = oMapper.convertValue(freshDeskConversationPojo, Map.class);
		JsonNode response = null;
		if (attachments != null) {
			for (AttachmentPojo attachmentPojo : attachments) {
				attachmentPojo.setConversationId(conversationPojo);
			}
			List<File> files;
			try {
				files = ticketService.downloadAttachments(attachments);
				response = freshDeskClient.replyToTicket(freshDeskTicketId, map, files);
				FileUtils.deleteFiles(files);
			} catch (S3DownloadException e) {
				log.error("Attachemnt download failed so proceeding without attachment");
				e.printStackTrace();
				response = freshDeskClient.replyToTicket(freshDeskTicketId, map, null);
			}
			
		} else {
			response = freshDeskClient.replyToTicket(freshDeskTicketId, map, null);
		}
		Map<String,Object> conversationResponse = oMapper.readValue(response.toString(), Map.class);
		conversationPojo.setAgent(false);
		conversationPojo.setTicketId(ticketPojo);
		conversationPojo.setPrivateNote(false);
		Object freshdeskConversationId = conversationResponse.get("id");
		if (freshdeskConversationId instanceof Long) {
			conversationPojo.setFreshDeskConversationId((long) freshdeskConversationId);
		} else if (freshdeskConversationId instanceof Integer) {
			Integer freshdeskConversationIdInt = (Integer) freshdeskConversationId;
			conversationPojo.setFreshDeskConversationId(new Long(freshdeskConversationIdInt));
		}
		if (ticketPojo.getStatus() == 4 || ticketPojo.getStatus() == 5) {
			ticketPojo.setStatus(2);
			ticketRepository.save(ticketPojo);
		}
		ConversationPojo conversationPojo2 = conversationRepository.save(conversationPojo);
		return conversationPojo2;
	}
}
