package com.niyo.customersupport.services;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.clients.NiyoAppClient;
import com.niyo.customersupport.exceptions.CorporateCreationFailedException;
import com.niyo.customersupport.pojos.CompanyPojo;
import com.niyo.customersupport.pojos.CustomerPojo;
import com.niyo.customersupport.pojos.TicketPojo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@EnableAsync
public class FreshDeskCustomerService {
	
	@Autowired
	private NiyoAppClient niyoAppClient;
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private CompanyService companyService;

	@Async
	public void updateCustomerInfoInFreshDesk(JsonNode customerDetailsResponse, long freshDeskRequesterId) throws UnirestException {
		JSONArray responseArray = customerDetailsResponse.getArray();
		if (responseArray.length() != 1) {
			return;
		}
		JSONObject niyoCustomerDetail;
		try {
			niyoCustomerDetail = responseArray.getJSONObject(0);
		} catch (JSONException e) {
			log.error("Niyo customer details not found");
			return;
		}
//		Long freshDeskCustomerID = null;
//		try {
//			freshDeskCustomerID = niyoCustomerDetail.getLong("freshDeskCustId");
//		} catch (JSONException e) {
//			log.debug("freshdesk customer id not found in niyo");
//		}
//		if (freshDeskCustomerID != null && !freshDeskCustomerID.equals(new Long(0))) {
//			return;
//		}
		Map<String,Object> request = new HashMap<>();
		JsonNode freshDeskResponse = freshDeskClient.getRequeterInfoFromFreshDeskByRequetserId(freshDeskRequesterId);
		JSONObject freshDeskCustomerDetails = freshDeskResponse.getObject();
		String freshdeskCustomerName = null;
		try {
			freshdeskCustomerName = freshDeskCustomerDetails.getString("name");
		} catch (JSONException e) {
			log.debug("Name not available in freshdesk" + e.getMessage());
		}
		String niyoCustomerName = null;
		try {
			niyoCustomerName = niyoCustomerDetail.getString("firstName");
		} catch (JSONException e) {
			log.debug("Name not available in niyo" + e.getMessage());
		}
		if (niyoCustomerName != null) {
			if (freshdeskCustomerName == null) {
				request.put("name", niyoCustomerName);
			} else {
				if (!freshdeskCustomerName.equals(niyoCustomerName)) {
					request.put("name", niyoCustomerName);
				}
			}
		}
		String freshDeskCustPhoneNo = null;
		try {
			freshDeskCustPhoneNo = freshDeskCustomerDetails.getString("phone");
		} catch (JSONException e) {
			log.debug("mobile number not available in freshdesk");
		}
		JSONObject niyoPhoneObject = null;
		String niyoPhoneNo = null;
		try {
			niyoPhoneObject = niyoCustomerDetail.getJSONObject("phone");
			if (niyoPhoneObject != null) {
				niyoPhoneNo = niyoPhoneObject.getString("number");
			}
		} catch (JSONException e) {
			log.debug("Phone details not available in niyo");
		}
		if (niyoPhoneNo != null) {
			if (freshDeskCustPhoneNo == null) {
				request.put("phone", niyoPhoneNo);
			}
			else {
				if (!niyoPhoneNo.equals(freshDeskCustPhoneNo)) {
					request.put("phone", niyoPhoneNo);
				}
			}
		}
		String freshDeskCustEmailId = null;
		try {
			freshDeskCustEmailId = freshDeskCustomerDetails.getString("email");
		} catch (JSONException e) {
			log.error("Email details not available in freshdesk");
		}
		JSONObject niyoEmailObject = null;
		String niyoEmailId = null;
		try {
			niyoEmailObject = niyoCustomerDetail.getJSONObject("email");
			if (niyoEmailObject != null) {
				niyoEmailId = niyoEmailObject.getString("email");
			}
		} catch (JSONException e) {
			log.error("Email details not available in niyo");
		}
		
		if (niyoEmailId != null) {
			if (freshDeskCustEmailId == null) {
				request.put("email", niyoEmailId);
			} else {
				if (!niyoEmailId.equals(freshDeskCustEmailId)) {
					request.put("email", niyoEmailId);
				}
			}
		}
		JSONObject freshDeskCustCustomFields = null;
		String freshDeskCustCrn = null;
		try {
			freshDeskCustCustomFields = freshDeskCustomerDetails.getJSONObject("custom_fields");
			if (freshDeskCustCustomFields != null) {
				freshDeskCustCrn = freshDeskCustCustomFields.getString("crn");
			}
		} catch (JSONException e) {
			log.error("no crn details available in freshdesk");
		}
		String niyoCrn = null;
		try {
			niyoCrn = niyoCustomerDetail.getString("crn");
		} catch (JSONException e) {
			log.error("no crn details available in Niyo");
		}
		if (niyoCrn != null) {
			if (freshDeskCustCrn == null) {
				Map<String,String> customFields = new HashMap<>();
				customFields.put("crn", niyoCrn);
				request.put("[custom_fields][crn]", niyoCrn);
			} else {
				if (!niyoCrn.equals(freshDeskCustCrn)) {
					Map<String,String> customFields = new HashMap<>();
					customFields.put("crn", niyoCrn);
					request.put("[custom_fields][crn]", niyoCrn);
				}
			}
		}
		
		JSONObject currentEmployerDetails = null;
		String corporateId = null;
		String corporateName = null;
		try {
			currentEmployerDetails = niyoCustomerDetail.getJSONObject("currentEmployer");
			if (currentEmployerDetails != null) {
				corporateId = currentEmployerDetails.getString("corporateId");
				corporateName = currentEmployerDetails.getString("name");
			}
		} catch (JSONException e1) {
			log.error("Employer information not found for " + niyoEmailId);
		}
		Long companyId = null;
		if (corporateId != null && !corporateId.isEmpty()) {
			try {
				companyId = companyService.createCompanyIfNotExists(corporateId, corporateName);
			} catch (CorporateCreationFailedException e) {
				log.error("Company creation in freshdesk failed");
			}
		}
		Long freshdeskCompanyId = null;
		try {
			freshdeskCompanyId = freshDeskCustomerDetails.getLong("company_id");
		} catch (JSONException e1) {
			log.debug("Company Id not available in freshdesk");
		}
		if (companyId != null) {
			if (freshdeskCompanyId == null) {
				request.put("company_id", companyId);
			} else if (!companyId.equals(freshdeskCompanyId)) {
				request.put("company_id", companyId);
			}
		}
		if (request.size() > 0) {
			freshDeskClient.updateCustomerInfo(freshDeskRequesterId, request);
		}
		try {
			niyoAppClient.addFreshdeskIdIntoCustomer(niyoCrn, freshDeskRequesterId);
		} catch (JSONException e) {
			log.error("Failed to add freshdesk customer id into freshdesk");
		}
	}
	
	@Async
	public void updateCustomerInfoInFreshdesk(Long freshdeskCustomerId, CustomerPojo customerPojo) throws UnirestException {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> body = mapper.convertValue(customerPojo, Map.class);
		if (body.isEmpty()) {
			return;
		}
		freshDeskClient.updateCustomerInfo(freshdeskCustomerId, body);
	}
	
	public CustomerPojo getCustomerPojoByPhoneOrEmailOrCrn(String parameter, String parameterValue
			, CompanyPojo companyPojo) throws UnirestException {
		CustomerPojo customerPojo = new CustomerPojo();
		JsonNode customerDetailsResponse = niyoAppClient.getCustomerDetailByParameter(parameter, parameterValue);
		if (customerDetailsResponse != null) {
			JSONArray responseArray = customerDetailsResponse.getArray();
			if (responseArray.length() != 1) {
				return null;
			}
			JSONObject niyoCustomerDetail = null;
			try {
				niyoCustomerDetail = responseArray.getJSONObject(0);
				if (niyoCustomerDetail == null) {
					return null;
				}
			} catch (JSONException e) {
				log.error("Customer details not found");
			}
			
			String niyoCustomerName = null;
			try {
				niyoCustomerName = niyoCustomerDetail.getString("firstName");
			} catch (JSONException e) {
				log.error("Customer name not found");
			}
			if (niyoCustomerName != null) {
				customerPojo.setName(niyoCustomerName);
			}
			JSONObject niyoPhoneObject = null;
			try {
				niyoPhoneObject = niyoCustomerDetail.getJSONObject("phone");
			} catch (JSONException e) {
				log.error("Customer phone details not found");
			}
			String niyoPhoneNo;
			try {
				niyoPhoneNo = niyoPhoneObject.getString("number");
				if (niyoPhoneNo != null) {
					customerPojo.setPhone(niyoPhoneNo);
				}
			} catch (JSONException e) {
				log.error("Customer phone number details not found");
			}
			
			JSONObject niyoEmailObject;
			try {
				niyoEmailObject = niyoCustomerDetail.getJSONObject("email");
				if (niyoEmailObject != null) {
					String niyoEmailId = niyoEmailObject.getString("email");
					if (niyoEmailId != null) {
						customerPojo.setEmail(niyoEmailId);
					}
				}
			} catch (JSONException e) {
				log.error("Customer email details not found");
			}
			
			String niyoCrn;
			try {
				niyoCrn = niyoCustomerDetail.getString("crn");
				if (niyoCrn != null) {
					customerPojo.setCrn(niyoCrn);
				}
			} catch (JSONException e) {
				log.error("Customer crn details not found");
			}
			try {
				JSONObject currentEmployerDetails = niyoCustomerDetail.getJSONObject("currentEmployer");
				if (currentEmployerDetails != null) {
					String corporateId = currentEmployerDetails.getString("corporateId");
					String corporateName = currentEmployerDetails.getString("name");
					companyPojo.setCorporateId(corporateId);
					companyPojo.setDescription(corporateName);
				}
			} catch (JSONException e) {
				log.error("Current employer details not found");
			}
		}
		return customerPojo;
	}
	
	@Async
	public void updateCustomerInfoInFreshdeskFromNiyo(String parameter, String parameterValue, Long freshdeskCustomerId) throws UnirestException {
		CompanyPojo companyPojo = new CompanyPojo();
		CustomerPojo customerPojo = getCustomerPojoByPhoneOrEmailOrCrn(parameter, parameterValue, companyPojo);
		String corporateId = companyPojo.getCorporateId();
		String corporateName = companyPojo.getDescription();
		if (corporateId != null && !corporateId.isEmpty()) {
			try {
				Number companyId = companyService.createCompanyIfNotExists(corporateId, corporateName);
				customerPojo.setCompanyId(companyId);
			} catch (CorporateCreationFailedException e) {
				log.error("Corporate creation in freshdesk failed updating customer without adding it to company");
			}
		}
		updateCustomerInfoInFreshdesk(freshdeskCustomerId, customerPojo);
		
	}
	
	public CustomerPojo getCustomerPojoFromTicketPojo(TicketPojo ticketPojo) {
		CustomerPojo customerPojo = new CustomerPojo();
		customerPojo.setCrn(ticketPojo.getCrn());
		String name = ticketPojo.getName();
		if (name != null && !name.isEmpty()) {
			customerPojo.setName(ticketPojo.getName());
		}
		return customerPojo;
	}
	
//	@Async
//	public void updateCustomerInfoInFreshdesk(TicketPojo ticketPojo, Long freshdeskCustomerId) throws UnirestException {
//		CustomerPojo customerPojo = getCustomerPojoFromTicketPojo(ticketPojo);
//		updateCustomerInfoInFreshdesk(freshdeskCustomerId, customerPojo);
//	}
}
