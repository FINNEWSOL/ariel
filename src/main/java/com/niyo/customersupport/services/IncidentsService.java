package com.niyo.customersupport.services;

import java.io.IOException;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.clients.NiyoAppClient;
import com.niyo.customersupport.enums.AgentGroup;
import com.niyo.customersupport.enums.PartnerBank;
import com.niyo.customersupport.enums.TicketCategory;
import com.niyo.customersupport.enums.TicketPriority;
import com.niyo.customersupport.enums.TicketSource;
import com.niyo.customersupport.enums.TicketStatus;
import com.niyo.customersupport.enums.TicketType;
import com.niyo.customersupport.enums.TransactionCategory;
import com.niyo.customersupport.enums.TransactionType;
import com.niyo.customersupport.exceptions.CorporateCreationFailedException;
import com.niyo.customersupport.exceptions.EmployeeNotFoundException;
import com.niyo.customersupport.pojos.CardTransaction;
import com.niyo.customersupport.pojos.CustomerPojo;
import com.niyo.customersupport.pojos.FreshDeskTicketPojo;
import com.niyo.customersupport.pojos.FundTransfer;
import com.niyo.customersupport.pojos.NiyoCustomerPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.repositories.TicketRepository;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IncidentsService {
	
	@Autowired
	private NiyoAppClient niyoAppClient;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private FreshDeskCustomerService freshDeskCustomerService;
	
	@Autowired
	private LogService logService;

	public void createIncidenceForTransactionFailure (CardTransaction cardTransaction, Channel channel, long deliveryTag) throws IOException {
		String crn = cardTransaction.getCrn();
		TicketPojo ticketPojo = null;
		try {
			ticketPojo = getTicketPojoFromCrn(crn);
		} catch (UnirestException | IOException | JSONException | EmployeeNotFoundException e) {
			ticketPojo = getTicketPojoWithDummyData(ticketPojo,crn,TicketType.TRANSACTION_DECLINED.getValue());
			log.debug("Error getting employee informatin");
			logService.saveLog("Getting employee informatin from niyo", "Crn: " + crn, "failed");
		}
		ticketPojo = getTicketDescriptionFromCardTransaction(cardTransaction, ticketPojo, crn);
		ticketPojo.setPriority(TicketPriority.LOW.getValue());
		ticketPojo.setStatus(TicketStatus.OPEN.getValue());
		ticketPojo.setSource(TicketSource.MOBIHELP.getValue());
		ticketPojo.setGroupId(AgentGroup.CUSTOMER_SUPPORT.getValue());
		FreshDeskTicketPojo freshDeskTicketPojo = dozerBeanMapper.map(ticketPojo, FreshDeskTicketPojo.class);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.convertValue(freshDeskTicketPojo, Map.class);
		JsonNode response = null;
		try {
			response = freshDeskClient.createTicketInFreshDeskWithAttachment(map,null);
		} catch (JsonProcessingException | UnirestException e) {
			channel.basicReject(deliveryTag, true);
			log.debug("Error creating ticket in freshdesk");
			logService.saveLog("ticket creation in freshdesk", "Request: " + map.toString(), "failed");
		}
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		FreshDeskTicketPojo ticketResponsePojo = mapper.readValue(response.toString(), FreshDeskTicketPojo.class);
		Long freshdeskRequesterId = ticketResponsePojo.getFreshDeskRequesterId();
		ticketPojo.setFreshdeskTicketId(ticketResponsePojo.getId());
		ticketPojo.setFreshDeskRequesterId(ticketResponsePojo.getFreshDeskRequesterId());
		//ticketRepository.save(ticketPojo);
		Long companyId = null;
		String corporateId = ticketPojo.getCorporateid();
		String corporateName = ticketPojo.getCorporateName();
		try {
			if (corporateId != null && corporateName != null) {
				companyId = companyService.createCompanyIfNotExists(corporateId, corporateName);
			}
		} catch (CorporateCreationFailedException e) {
			log.debug("Error creating company in freshdesk");
			logService.saveLog("company creation in freshdesk", "Request: CorportaeId: " + corporateId + " CorporateName: " + corporateName, "failed");
		}
		if (companyId != null) {
			CustomerPojo customerPojo = new CustomerPojo();
			customerPojo.setCompanyId(companyId);
			try {
				freshDeskCustomerService.updateCustomerInfoInFreshdesk(freshdeskRequesterId, customerPojo);
			} catch (UnirestException e) {
				log.debug("Error updating customer information in freshdesk");
				logService.saveLog("customer updation in freshdesk", "freshdeskRequesterId: " + freshdeskRequesterId, "failed");
			}
		}
	}
	
	public void createIncidenceForTransferFailure (FundTransfer fundTransfer, Channel channel, long deliveryTag) throws IOException {
		String crn = fundTransfer.getCrn();
		TicketPojo ticketPojo = null;
		try {
			ticketPojo = getTicketPojoFromCrn(crn);
		} catch (UnirestException | IOException | JSONException | EmployeeNotFoundException e) {
			ticketPojo = getTicketPojoWithDummyData(ticketPojo,crn,TicketType.TRANSFER_FAILED.getValue());
			log.debug("Error getting employee informatin");
			logService.saveLog("Getting employee informatin from niyo", "Crn: " + crn, "failed");
		}
		ticketPojo = getTicketPojoFromFundTransfer(fundTransfer, ticketPojo, crn);
		ticketPojo.setPriority(TicketPriority.LOW.getValue());
		ticketPojo.setStatus(TicketStatus.OPEN.getValue());
		ticketPojo.setSource(TicketSource.MOBIHELP.getValue());
		ticketPojo.setGroupId(AgentGroup.CUSTOMER_SUPPORT.getValue());
		FreshDeskTicketPojo freshDeskTicketPojo = dozerBeanMapper.map(ticketPojo, FreshDeskTicketPojo.class);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = mapper.convertValue(freshDeskTicketPojo, Map.class);
		JsonNode response = null;
		try {
			response = freshDeskClient.createTicketInFreshDeskWithAttachment(map,null);
		} catch (JsonProcessingException | UnirestException e) {
			channel.basicReject(deliveryTag, true);
			log.debug("Error creating ticket in freshdesk");
			logService.saveLog("ticket creation in freshdesk", "Request: " + map.toString(), "failed");
		}
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		FreshDeskTicketPojo ticketResponsePojo = mapper.readValue(response.toString(), FreshDeskTicketPojo.class);
		Long freshdeskRequesterId = ticketResponsePojo.getFreshDeskRequesterId();
		ticketPojo.setFreshdeskTicketId(ticketResponsePojo.getId());
		ticketPojo.setFreshDeskRequesterId(ticketResponsePojo.getFreshDeskRequesterId());
		//ticketRepository.save(ticketPojo);
		String corporateId = ticketPojo.getCorporateid();
		String corporateName = ticketPojo.getCorporateName();
		Long companyId = null;
		try {
			companyId = companyService.createCompanyIfNotExists(ticketPojo.getCorporateid(), ticketPojo.getName());
		} catch (CorporateCreationFailedException e) {
			log.debug("Error creating company in freshdesk");
			logService.saveLog("company creation in freshdesk", "Request: CorportaeId: " + corporateId + " CorporateName: " + corporateName, "failed");
		}
		if (companyId != null) {
			CustomerPojo customerPojo = new CustomerPojo();
			customerPojo.setCompanyId(companyId);
			try {
				freshDeskCustomerService.updateCustomerInfoInFreshdesk(freshdeskRequesterId, customerPojo);
			} catch (UnirestException e) {
				log.debug("Error updating customer information in freshdesk");
				logService.saveLog("customer updation in freshdesk", "freshdeskRequesterId: " + freshdeskRequesterId, "failed");
			}
		}
	}
	
	public TicketPojo getTicketPojoFromCrn(String crn) throws UnirestException, JsonParseException, JsonMappingException, IOException, JSONException, EmployeeNotFoundException {
		TicketPojo ticketPojo = new TicketPojo();
		ticketPojo.setCrn(crn);
		JsonNode response = niyoAppClient.getCustomerDetailByParameter("crn", crn);
		JSONArray responseArray = response.getArray();
		Object customerDetails = responseArray.get(0);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		NiyoCustomerPojo customerPojo = mapper.readValue(customerDetails.toString(), NiyoCustomerPojo.class);
		if (!customerPojo.isValid()) {
			throw new EmployeeNotFoundException();
		}
		if (customerPojo.getFirstName() != null) {
			ticketPojo.setName(customerPojo.getFirstName());
		} else if (customerPojo.getNickName() != null) {
			ticketPojo.setName(customerPojo.getNickName());
		}
		Map<String,String> email = customerPojo.getEmail();
		if (email != null) {
			String emailId = email.get("email");
			if (emailId != null && !emailId.isEmpty()) {
				ticketPojo.setEmail(emailId);
			}
		}
		Map<String,String> phone = customerPojo.getPhone();
		if (phone != null) {
			String number = phone.get("number");
			if (number != null && !number.isEmpty()) {
				ticketPojo.setPhone(number);
			}
		}
		Map<String,String> currentEmployer = customerPojo.getCurrentEmployer();
		String corporateId = null;
		String corporateName = null;
		if (currentEmployer != null) {
			corporateId = currentEmployer.get("corporateId");
			corporateName = currentEmployer.get("name");
			if (corporateId != null && !corporateId.isEmpty()) {
				ticketPojo.setCorporateid(corporateId);
			}
			if (corporateName != null && !corporateName.isEmpty()) {
				ticketPojo.setCorporateName(corporateName);
			}
		}
		Map<String, String> niyoAccount = customerPojo.getNiyoAccount();
		if (niyoAccount != null) {
			String bankName = niyoAccount.get("bankName");
			if (bankName != null) {
				if (bankName.equalsIgnoreCase("yes bank")) {
					ticketPojo.setPartnerBank(PartnerBank.YES_BANK.getValue());
				} else if (bankName.equalsIgnoreCase("dcb bank")) {
					ticketPojo.setPartnerBank(PartnerBank.DCB_BANK.getValue());
				}
			}
		}
		return ticketPojo;
	}
	
	public TicketPojo getTicketDescriptionFromCardTransaction(CardTransaction cardTransaction, TicketPojo ticketPojo, String crn) {
		ticketPojo.setSubject("System ticket transaction failed crn: " + crn + " corporateId: " + cardTransaction.getCorporateCode());
		ticketPojo.setCategory(TicketCategory.TRANSACTION_DECLINED.getValue());
		ticketPojo.setType(TicketType.TRANSACTION_DECLINED.getValue());
		String description = "Transaction Details \n Merchant: " +cardTransaction.getMerchant_name() + "\n MCC code: " + cardTransaction.getMcc_code() + 
				"\n Merchant Category: " + cardTransaction.getMerchantCategory() + 
				"\n Pocket: " + cardTransaction.getPocket() + " \n Product: " + cardTransaction.getProductCode() + 
				"\n Status code: " + cardTransaction.getResponse_code() + "\n Status description: " + cardTransaction.getResponseDescription() +
				"\n Description: " + cardTransaction.getDescription() + "\n Billing Amount: " + cardTransaction.getBilling_amount() + 
				"\n Transaction Amount: " + cardTransaction.getTxn_amount() + "\n Closing Pocket Balance: " + cardTransaction.getBilledPocketBalance() +
				"\n Closing Card Balance: " + cardTransaction.getCardBalance() +
				"\n CR/DR: " + cardTransaction.getTransactionType() + 
				"\n Corporate code: " + cardTransaction.getCorporateCode();
		ticketPojo.setDescription(description);
		TransactionType transactionType = cardTransaction.getTransactionType();
		if (transactionType != null && transactionType.equals(TransactionType.D)) {
			ticketPojo.setTransactionType(TransactionCategory.SPENDS.toString());
		} else if (transactionType != null && transactionType.equals(TransactionType.C)) {
			ticketPojo.setTransactionType(TransactionCategory.INCOMES.toString());
		}
		ticketPojo.setTransactionId(cardTransaction.getId());
		return ticketPojo;
	}
	
	public TicketPojo getTicketPojoFromFundTransfer(FundTransfer fundTransfer, TicketPojo ticketPojo, String crn) {
		ticketPojo.setSubject("System ticket transfer failed crn: " + crn + " corporateId: " + ticketPojo.getCorporateid());
		ticketPojo.setCategory(TicketCategory.FUND_TRANSFER.getValue());
		ticketPojo.setType(TicketType.TRANSFER_FAILED.getValue());
		String description = "Transfer Details \n fundTransferId: " + fundTransfer.getFundTransferId() + 
				"\n crn: " + crn + "\n status: " + fundTransfer.getStatus() + "\n Fund transfer reference Id: " + fundTransfer.getRefID() +
				"\n beneficiary Id: " + fundTransfer.getBeneficiaryId() + "\n Network type: " + fundTransfer.getNetworkType();
		ticketPojo.setDescription(description);
		return ticketPojo;
	}
	
	public TicketPojo getTicketPojoWithDummyData(TicketPojo ticketPojo, String crn, String type) {
		if (ticketPojo == null) {
			ticketPojo = new TicketPojo();
		}
		ticketPojo.setCrn(crn);
		ticketPojo.setName("Incident");
		ticketPojo.setType(type);
		ticketPojo.setEmail("dummyincidentemail@goniyo.com");
		return ticketPojo;
	}
}
