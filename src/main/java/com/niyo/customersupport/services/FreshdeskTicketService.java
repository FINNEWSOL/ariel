package com.niyo.customersupport.services;

import java.io.IOException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.clients.NiyoAppClient;
import com.niyo.customersupport.enums.AgentGroup;
import com.niyo.customersupport.enums.PartnerBank;
import com.niyo.customersupport.pojos.NiyoCustomerPojo;
import com.niyo.customersupport.pojos.TicketPojo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@EnableAsync
public class FreshdeskTicketService {
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private NiyoAppClient niyoAppClient;

	@Async
	public void updateTicket(TicketPojo ticketPojo, Integer freshdeskTicketId) throws UnirestException, IOException {
		freshDeskClient.updateTicket(ticketPojo, freshdeskTicketId);
	}
	
	@Async
	public void updateTicket(String crn, Integer freshdeskTicketId) {
		TicketPojo ticketPojo = new TicketPojo();
		ticketPojo.setGroupId(AgentGroup.CUSTOMER_SUPPORT.getValue());
		JsonNode customerDetailsResponse = null;
		try {
			customerDetailsResponse = niyoAppClient.getCustomerDetailByParameter("crn", crn);
		} catch (UnirestException e) {
			log.error("Error getting customer information from niyo");
			e.printStackTrace();
			return;
		}
		if (customerDetailsResponse != null) {
			JSONArray customerDetails = customerDetailsResponse.getArray();
			if (customerDetails.length() < 2) {
				JSONObject customerDetail;
				try {
					customerDetail = customerDetails.getJSONObject(0);
				} catch (JSONException e) {
					log.error("Invalid response for getting customer from niyo");
					e.printStackTrace();
					return;
				}
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				NiyoCustomerPojo customerPojo;
				try {
					customerPojo = mapper.readValue(customerDetail.toString(), NiyoCustomerPojo.class);
				} catch (IOException e) {
					log.error("Invalid response for getting customer from niyo");
					e.printStackTrace();
					return;
				}
				if (customerPojo == null) {
					return;
				}
				if (customerPojo.isValid()) {
					if (crn != null) {
						ticketPojo.setCrn(crn);
					}
					Map<String,String> currentEmployer = customerPojo.getCurrentEmployer();
					String corporateId = null;
					String corporateName = null;
					if (currentEmployer != null) {
						corporateId = currentEmployer.get("corporateId");
						corporateName = currentEmployer.get("name");
						if (corporateId != null && !corporateId.isEmpty()) {
							ticketPojo.setCorporateid(corporateId);
						}
						if (corporateName != null && !corporateName.isEmpty()) {
							ticketPojo.setCorporateName(corporateName);
						}
					}
					Map<String, String> niyoAccount = customerPojo.getNiyoAccount();
					if (niyoAccount != null) {
						String bankName = niyoAccount.get("bankName");
						if (bankName != null) {
							if (bankName.equalsIgnoreCase("yes bank")) {
								ticketPojo.setPartnerBank(PartnerBank.YES_BANK.getValue());
							} else if (bankName.equalsIgnoreCase("dcb bank")) {
								ticketPojo.setPartnerBank(PartnerBank.DCB_BANK.getValue());
							}
						}
					}
				}	
			}
		}
		try {
			updateTicket(ticketPojo, freshdeskTicketId);
		} catch (UnirestException | IOException e) {
			log.error("Ticket updation failed");
			e.printStackTrace();
			return;
		}
	}
}
