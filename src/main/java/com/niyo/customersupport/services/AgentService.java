package com.niyo.customersupport.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.pojos.AgentPojo;
import com.niyo.customersupport.pojos.Contact;


@Service
public class AgentService {

	@Autowired(required=true)
	private FreshDeskClient freshDeskClient;
	
	private static Map<Long,String> agentDetails;
	
	@Scheduled(fixedRate=1800000) //Agent details will be loaded after every 30 minutes
	public void loadAgentMappings() throws UnirestException, JsonParseException, JsonMappingException, IOException {
		JsonNode jsonNode = freshDeskClient.getAgentDetails();
		if(agentDetails == null) {
			agentDetails = new HashMap<Long, String>();
		}
		JSONArray jsonArray = jsonNode.getArray();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<AgentPojo> agents = mapper.readValue(jsonArray.toString(), new TypeReference<List<AgentPojo>>() { });
		for (AgentPojo agent : agents) {
			Long id = agent.getId();
			Contact contact = agent.getContact();
			String name = contact.getName();
			if (id != null && name != null) {
				agentDetails.put(id, name);
			}
		}
	}
	
	public String getAgentNameById(Long id) throws JsonParseException, JsonMappingException, UnirestException, IOException {
		if (agentDetails == null || agentDetails.get(id) == null) {
			loadAgentMappings();
		}
		String name = agentDetails.get(id);
		return name;
	}
}
