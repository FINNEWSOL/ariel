package com.niyo.customersupport.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.transfer.Download;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.adapter.AttachmentAdapter;
import com.niyo.customersupport.adapter.TicketAdapter;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.clients.NiyoAppClient;
import com.niyo.customersupport.config.aws.Credentials;
import com.niyo.customersupport.enums.AgentGroup;
import com.niyo.customersupport.enums.PartnerBank;
import com.niyo.customersupport.enums.TicketCategory;
import com.niyo.customersupport.enums.TicketType;
import com.niyo.customersupport.exceptions.EmployeeNotFoundException;
import com.niyo.customersupport.exceptions.InvalidMobileNumberException;
import com.niyo.customersupport.pojos.AttachmentPojo;
import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.FreshDeskTicketPojo;
import com.niyo.customersupport.pojos.NiyoCustomerPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.pojos.UpdateStatusRequestPojo;
import com.niyo.customersupport.repositories.TicketRepository;
import com.niyo.customersupport.utils.CheckTicketStatus;
import com.niyo.customersupport.utils.FileUtils;
import com.niyo.customersupport.utils.FreshDeskUtils;
import com.niyo.customersupport.utils.StringUtils;
import com.niyo.customersupport.utils.awsutils.AwsUtils;
import com.niyo.customersupport.utils.restutils.exceptions.S3DownloadException;
import com.niyo.customersupport.utils.restutils.exceptions.TicketAlreadyExistsException;
import com.niyo.customersupport.utils.restutils.exceptions.TicketNotFoundException;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class TicketService implements CheckTicketStatus {
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private AwsUtils awsUtils;
	
	@Autowired
	private Credentials credentials;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private NiyoAppClient niyoAppClient;
	
	@Autowired
	private AttachmentAdapter attachmentAdapter;
	
	@Autowired
	private FreshDeskCustomerService freshDeskCustomerService;
	
	@Autowired
	private FreshDeskUtils freshDeskUtils;
	
	@Autowired
	private ConversationService conversationService;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private FreshdeskTicketService freshdeskTicketService;

	public TicketPojo createTicketInFreshDesk(TicketPojo ticketPojo) throws UnirestException, IOException, JSONException {
		ticketPojo.setStatus(2);
		ticketPojo.setPriority(2);
		ticketPojo.setSource(2);
		ticketPojo.setCategory(ticketPojo.getSubject());
		List<AttachmentPojo> attachments = ticketPojo.getAttachments();
		FreshDeskTicketPojo freshDeskTicketPojo = dozerBeanMapper.map(ticketPojo, FreshDeskTicketPojo.class);
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> map = oMapper.convertValue(freshDeskTicketPojo, Map.class);
		JsonNode response = null;
		if (attachments != null) {
			for (AttachmentPojo attachmentPojo : attachments) {
				attachmentPojo.setTicketId(ticketPojo);
			}
			List<File> files;
			try {
				files = downloadAttachments(attachments);
				response = freshDeskClient.createTicketInFreshDeskWithAttachment(map,files);
				FileUtils.deleteFiles(files);
			} catch (S3DownloadException e) {
				log.error("Attachment download failed so creating ticket without attachment");
				e.printStackTrace();
				response = freshDeskClient.createTicketInFreshDeskWithAttachment(map,null);
			}
			
		} else {
			response = freshDeskClient.createTicketInFreshDeskWithAttachment(map,null);
		}
		JSONObject ticketResponse = response.getObject();
		Object freshdeskTicketId = ticketResponse.get("id");
		if (freshdeskTicketId instanceof Integer) {
			ticketPojo.setFreshdeskTicketId((Integer)freshdeskTicketId);
		}
		Object freshdeskRequesterId = ticketResponse.get("requester_id");
		if (freshdeskRequesterId instanceof Long) {
			ticketPojo.setFreshDeskRequesterId((long) freshdeskRequesterId);
		}
		if (freshdeskRequesterId instanceof Integer) {
			Integer freshdeskRequesterIdInt = (Integer) freshdeskRequesterId;
			ticketPojo.setFreshDeskRequesterId(new Long(freshdeskRequesterIdInt));
		} 
		TicketPojo ticketPojo2 = ticketRepository.save(ticketPojo);
		String mobileNumber = ticketPojo2.getPhone();
		if (mobileNumber != null) {
			freshDeskCustomerService.updateCustomerInfoInFreshdeskFromNiyo("mobile", mobileNumber, ticketPojo2.getFreshDeskRequesterId());
		}
		freshdeskTicketService.updateTicket(ticketPojo.getCrn(), ticketPojo.getFreshdeskTicketId());
		return ticketPojo2;
	}
	
	public TicketPojo createTicket(TicketPojo ticketPojo) throws UnirestException, IOException, JSONException, TicketAlreadyExistsException {
		Integer freshDeskTicketId = ticketPojo.getFreshdeskTicketId();
		ticketPojo.setGroupId(AgentGroup.CUSTOMER_SUPPORT.getValue());
		TicketPojo existingTicketPojo = ticketRepository.findFirstByFreshdeskTicketId(freshDeskTicketId);
		if (existingTicketPojo != null) {
			throw new TicketAlreadyExistsException();
		}
		JsonNode response = freshDeskClient.getTicketDetaisInFreshDesk(freshDeskTicketId);
		JSONObject ticketResponse = response.getObject();
		JSONArray attachments = ticketResponse.getJSONArray("attachments");
		JsonNode customerDetailsResponse = niyoAppClient.getCustomerDetailByParameter("email", ticketPojo.getEmail());
		if (customerDetailsResponse != null) {
			JSONArray customerDetails = customerDetailsResponse.getArray();
			if (customerDetails.length() < 2) {
				JSONObject customerDetail = customerDetails.getJSONObject(0);
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				NiyoCustomerPojo customerPojo = mapper.readValue(customerDetail.toString(), NiyoCustomerPojo.class);
				if (customerPojo.isValid()) {
					String crn = customerPojo.getCrn();
					if (crn != null) {
						ticketPojo.setCrn(crn);
					}
					Map<String,String> currentEmployer = customerPojo.getCurrentEmployer();
					String corporateId = null;
					String corporateName = null;
					if (currentEmployer != null) {
						corporateId = currentEmployer.get("corporateId");
						corporateName = currentEmployer.get("name");
						if (corporateId != null && !corporateId.isEmpty()) {
							ticketPojo.setCorporateid(corporateId);
						}
						if (corporateName != null && !corporateName.isEmpty()) {
							ticketPojo.setCorporateName(corporateName);
						}
					}
					Map<String, String> niyoAccount = customerPojo.getNiyoAccount();
					if (niyoAccount != null) {
						String bankName = niyoAccount.get("bankName");
						if (bankName != null) {
							if (bankName.equalsIgnoreCase("yes bank")) {
								ticketPojo.setPartnerBank(PartnerBank.YES_BANK.getValue());
							} else if (bankName.equalsIgnoreCase("dcb bank")) {
								ticketPojo.setPartnerBank(PartnerBank.DCB_BANK.getValue());
							}
						}
					}
				}	
			}
		}
		if (attachments.length() != 0) {
			List<File> files = freshDeskUtils.downloadAttachmentFromFreshDesk(attachments);
			List<AttachmentPojo> attachmentPojos = attachmentAdapter.getAttachmentPojoFromFiles(files, ticketPojo);
			FileUtils.deleteFiles(files);
			ticketPojo.setAttachments(attachmentPojos);
		}
		TicketPojo ticketPojo2 = ticketRepository.save(ticketPojo);
		if (customerDetailsResponse != null) {
			freshDeskCustomerService.updateCustomerInfoInFreshDesk(customerDetailsResponse, ticketResponse.getLong("requester_id"));
			ticketPojo.setSubject(null);
			ticketPojo.setDescription(null);
			freshdeskTicketService.updateTicket(ticketPojo, ticketPojo.getFreshdeskTicketId());
		}
		return ticketPojo2;
	}
	
	public List<File> downloadAttachments(List<AttachmentPojo> attachments) throws S3DownloadException {
		List<File> downloadedFiles = new ArrayList<>();
		List<Download> downloads = new ArrayList<>();
		for (AttachmentPojo attachment : attachments) {
			String fileName = attachment.getFileName();
			String path = attachment.getKey();
			File file = new File(System.getProperty("java.io.tmpdir") + File.separator + fileName);
			Download download = awsUtils.downloadFile(path, file);
			try {
				download.waitForCompletion();
				downloadedFiles.add(file);
			} catch (AmazonClientException | InterruptedException e) {
				log.error("Download from s3 failed");
				e.printStackTrace();
				throw new S3DownloadException();
			}
		}
		return downloadedFiles;
	}
	
	public TicketPojo getTicketDetailsByTicketId(long ticketId) {
		TicketPojo ticketPojo = ticketRepository.findFirstByTicketId(ticketId);
		if (ticketPojo != null) {
			ticketPojo = TicketAdapter.removePrivateNotes(ticketPojo);
		}
		return ticketPojo;
	}
	
	public List<TicketPojo> getTicketsByCrn(String crn) {
		List<TicketPojo> ticketPojos = ticketRepository.findByCrn(crn);
		if (ticketPojos != null && ticketPojos.size() > 0) {
			ticketPojos = TicketAdapter.removePrivateNotes(ticketPojos);
		} 
		return ticketPojos;
	}
	
	public List<TicketPojo> getTicketsByCrn(String crn, Pageable page) {
		Page<TicketPojo> ticketPages = ticketRepository.findByCrn(crn, page);
		List<TicketPojo> ticketPojos = ticketPages.getContent();
		if (ticketPojos != null && ticketPojos.size() > 0) {
			ticketPojos = TicketAdapter.removePrivateNotes(ticketPojos);
		}
		return ticketPojos;
	}
	
	public String uploadTicketFilesToS3(String crn, File file) throws UnirestException, JSONException {
		String identityId = niyoAppClient.getIdentificationId(crn);
		String filePathInS3 = identityId + "-" + crn + File.separator + "tickets" + File.separator + new DateTime().getMillis() + File.separator + file.getName();
		awsUtils.uploadfile(credentials.getBucketname(), filePathInS3, file);
		return filePathInS3;
	}
	
	public TicketPojo createTicketFromExotel(String url, String registeredMobileNumber, String flowId, 
			String callReceivedFrom, Map<String,Object> allRequestParams,
			String partnerBank) 
					throws IllegalStateException, IOException, UnirestException, InvalidMobileNumberException {
		if (flowId.equals("3")) {
			if (niyoAppClient.blockCard(allRequestParams)) {
				return null;
			} else {
				TicketPojo ticketPojo = createTicketFromExotel(url, registeredMobileNumber, "3_failed", callReceivedFrom, allRequestParams, partnerBank);
				return ticketPojo;
			}
		}
		if(flowId.equals("4")) {
			String mobileNumber = StringUtils.removeLeadingAndTrealingQuotes(registeredMobileNumber);
			if (mobileNumber.length() != 10) {
				throw new InvalidMobileNumberException();
			}
			if (niyoAppClient.checkBalance(allRequestParams)) {
				return null;
			} else {
				TicketPojo ticketPojo = createTicketFromExotel(url, registeredMobileNumber, "4_failed", callReceivedFrom, allRequestParams, partnerBank);
				return ticketPojo;
			}
		} 
		if (registeredMobileNumber != null) {
			registeredMobileNumber = StringUtils.removeLeadingAndTrealingQuotes(registeredMobileNumber);
			registeredMobileNumber = StringUtils.getTenDigitMobileNumber(registeredMobileNumber);
		}
		if (callReceivedFrom != null) {
			callReceivedFrom = StringUtils.removeLeadingAndTrealingQuotes(callReceivedFrom);
			callReceivedFrom = StringUtils.getTenDigitMobileNumber(callReceivedFrom);
		}
		
		TicketPojo ticketPojo = new TicketPojo();
		ticketPojo.setType(TicketType.IVR.getValue());
		ticketPojo = getTicketPojoWithBasicInfo(ticketPojo, flowId, registeredMobileNumber, callReceivedFrom, url);
		ticketPojo.setCategory(ticketPojo.getSubject());
		if (partnerBank.equals("ybl")) {
			ticketPojo.setPartnerBank(PartnerBank.YES_BANK.getValue());
		} else if (partnerBank.equals("dcb")) {
			ticketPojo.setPartnerBank(PartnerBank.DCB_BANK.getValue());
		}
		ticketPojo.setGroupId(AgentGroup.CUSTOMER_SUPPORT.getValue());
		if (registeredMobileNumber == null) {
			ticketPojo = getTicketPojoWithCustomerDetails(ticketPojo, callReceivedFrom);
		} else {
			ticketPojo = getTicketPojoWithCustomerDetails(ticketPojo, registeredMobileNumber);
		}
		FreshDeskTicketPojo freshDeskTicketPojo = dozerBeanMapper.map(ticketPojo, FreshDeskTicketPojo.class);
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> map = oMapper.convertValue(freshDeskTicketPojo, Map.class);
		JsonNode freshDeskResponse = freshDeskClient.createTicketInFreshDeskWithAttachment(map,null);
		Long freshdeskRequesterId = null;
		if (freshDeskResponse != null) {
			JSONObject ticketResponse = freshDeskResponse.getObject();
			try {
				ticketPojo.setFreshdeskTicketId(new Integer(ticketResponse.getInt("id")));
				ticketPojo.setFreshDeskRequesterId(ticketResponse.getLong("requester_id"));
				freshdeskRequesterId = ticketResponse.getLong("requester_id");
			} catch (JSONException e) {
				log.error("Error creating ticket in freshdesk");
			}
		}
		ticketRepository.save(ticketPojo);
		if (registeredMobileNumber == null && callReceivedFrom != null) {
			freshDeskCustomerService.updateCustomerInfoInFreshdeskFromNiyo("mobile", callReceivedFrom, freshdeskRequesterId);
		} else if (registeredMobileNumber != null) {
			freshDeskCustomerService.updateCustomerInfoInFreshdeskFromNiyo("mobile", registeredMobileNumber, freshdeskRequesterId);
		}
		return ticketPojo;
	}
	
	public void reopenTicket(long ticketId, ConversationPojo conversationPojo) throws UnirestException, JSONException, IOException {
		conversationService.replyToTicket(ticketId, conversationPojo);
	}
	
	public TicketPojo updateStatus(Integer freshDeskTicketId, UpdateStatusRequestPojo updateStatusRequestPojo) throws TicketNotFoundException {
		String status = updateStatusRequestPojo.getStatus();
		TicketPojo ticketPojo = ticketRepository.findFirstByFreshdeskTicketId(freshDeskTicketId);
		if (ticketPojo == null) {
			throw new TicketNotFoundException();
		}
		int intStatus = checkStatus(status);
		ticketPojo.setStatus(intStatus);
		ticketRepository.save(ticketPojo);
		return ticketPojo;
	}
	
	public TicketPojo updatePriority(Integer freshDeskTicketId, UpdateStatusRequestPojo updateStatusRequestPojo) throws TicketNotFoundException {
		String priority = updateStatusRequestPojo.getPriority();
		TicketPojo ticketPojo = ticketRepository.findFirstByFreshdeskTicketId(freshDeskTicketId);
		if (ticketPojo == null) {
			throw new TicketNotFoundException();
		}
		int intPriority = checkPriority(priority);
		ticketPojo.setPriority(intPriority);
		ticketRepository.save(ticketPojo);
		return ticketPojo;
	}
	
	public TicketPojo getTicketPojoWithBasicInfo(TicketPojo ticketPojo, String flowId, String registeredMobileNumber, String callReceivedFrom, String url) {
		if (registeredMobileNumber != null) {
			ticketPojo.setPhone(registeredMobileNumber);
		} else {
			ticketPojo.setPhone(callReceivedFrom);
		}
		ticketPojo.setStatus(2);
		ticketPojo.setPriority(2);
		ticketPojo.setSource(3);
		if (flowId.equals("1")) {
			ticketPojo.setSubject(TicketCategory.OTHER.getValue());
			ticketPojo.setDescription("Support needed. \nCall received from: " + callReceivedFrom + 
					"\nHere is link for voicemail: " + url);
		}
		if (flowId.equals("2")) {
			ticketPojo.setSubject(TicketCategory.GENERAL_INFORMATION.getValue());
			ticketPojo.setDescription("Need details of niyo products. \nCall received from: " + callReceivedFrom + 
					"\n Here is link for voicemail: " + url);
		}
		if (flowId.equals("3_success")) {
			ticketPojo.setSubject(TicketCategory.CARD_LOST.getValue());
			ticketPojo.setDescription("Card lost. Card is locked successfully. \nCall received from: " + callReceivedFrom + 
					"\nRegistered mobile number: " + registeredMobileNumber );
			logService.saveLog("card lock", ticketPojo.getDescription(), "success");
		}
		if (flowId.equals("3_failed")) {
			ticketPojo.setSubject(TicketCategory.CARD_LOST.getValue());
			ticketPojo.setDescription("Card lost. Card lock operation failed. Call received from: " + callReceivedFrom + 
					"\nRegistered mobile number: " + registeredMobileNumber );
			logService.saveLog("card lock", ticketPojo.getDescription(), "failed");
		}
		if (flowId.equals("4_failed")) {
			ticketPojo.setSubject(TicketCategory.ACCOUNT_BALANCE.getValue());
			ticketPojo.setDescription("Balance check failed. Call received from: " + callReceivedFrom + 
					"\nRegistered mobile number: " + registeredMobileNumber );
			logService.saveLog("balance check", ticketPojo.getDescription(), "failed");
		}
		return ticketPojo;
	}
	
	public TicketPojo getTicketPojoWithCustomerDetails(TicketPojo ticketPojo, String phone) throws UnirestException {
		JsonNode customerDetailsResponse = niyoAppClient.getCustomerDetailByParameter("mobile", phone);
		if (customerDetailsResponse != null) {
			JSONArray niyoCustomerDetails = customerDetailsResponse.getArray();
			if (niyoCustomerDetails.length() < 2) {
				JSONObject niyoCustomerDetail = null;
				try {
					niyoCustomerDetail = niyoCustomerDetails.getJSONObject(0);
				} catch (JSONException e) {
					log.debug("No customer information for phone:" + phone);
				}
				String crn;
				try {
					crn = niyoCustomerDetail.getString("crn");
					ticketPojo.setCrn(crn);
				} catch (JSONException e) {
					log.debug("CRN not found");
				}
				
				String freshDeskCustomerID;
				try {
					freshDeskCustomerID = niyoCustomerDetail.getString("freshDeskCustId");
					if (freshDeskCustomerID != null && !freshDeskCustomerID.equals("0")) {
						ticketPojo.setFreshDeskRequesterId(Long.parseLong(freshDeskCustomerID));
					}
				} catch (JSONException e) {
					log.debug("freshdesk customer id not found");
				}
				
				String niyoCustomerName;
				try {
					niyoCustomerName = niyoCustomerDetail.getString("firstName");
					if (niyoCustomerName != null) {
						ticketPojo.setName(niyoCustomerName);
					}
				} catch (JSONException e) {
					log.debug("Name not found");
				}
				
				JSONObject niyoEmailObject;
				try {
					niyoEmailObject = niyoCustomerDetail.getJSONObject("email");
					String niyoEmailId = niyoEmailObject.getString("email");
					if (niyoEmailId != null) {
						ticketPojo.setEmail(niyoEmailId);
					}
				} catch (JSONException e) {
					log.debug("EmailId not found");
				}
				try {
					JSONObject employerInfo = niyoCustomerDetail.getJSONObject("currentEmployer");
					String corporateid = employerInfo.getString("corporateId");
					ticketPojo.setCorporateid(corporateid);
				} catch (JSONException e) {
					log.error("Employer information not found");
				}
			}
		} else {
			ticketPojo.setName("Exotel");
		}
		return ticketPojo;
	}
}
