package com.niyo.customersupport.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;
import com.niyo.customersupport.exceptions.CorporateCreationFailedException;
import com.niyo.customersupport.pojos.CompanyPojo;
import com.niyo.customersupport.repositories.CompanyRepository;

@Service
public class CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private FreshDeskClient freshDeskClient;
	
	@Autowired
	private LogService logService;

	public Long createCompanyIfNotExists(String corporateId, String corporateName) throws CorporateCreationFailedException {
		CompanyPojo companyPojo = companyRepository.findByCorporateId(corporateId);
		if (companyPojo != null) {
			return companyPojo.getId();
		}
		companyPojo = new CompanyPojo();
		companyPojo.setCorporateId(corporateId);
		companyPojo.setDescription(corporateName);
		try {
			CompanyPojo companyPojo2 = freshDeskClient.createCorporate(companyPojo);
			companyRepository.save(companyPojo2);
			return companyPojo2.getId();
		} catch (UnirestException | IOException e) {
			logService.saveLog("create corporate", "Createion of corporate in freshdesk failed" + companyPojo.toString(), "failed");
			throw new CorporateCreationFailedException("Corporate creation failed");
		}
	}
}
