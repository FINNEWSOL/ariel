package com.niyo.customersupport.services;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.NiyoAppClient;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StaticResourceService {

	public static String TOKEN;
	
	@Autowired
	private NiyoAppClient niyoAppClient;
	
	@Scheduled(fixedDelay=5900000)
	public void refreshToken() throws UnirestException {
		try {
			StaticResourceService.TOKEN = niyoAppClient.getToken();
		} catch (JSONException e) {
			log.error("JSON Parser exception");
			e.printStackTrace();
		}
	}
}
