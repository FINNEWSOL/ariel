package com.niyo.customersupport.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.niyo.customersupport.pojos.CategoryPojo;
import com.niyo.customersupport.repositories.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	public CategoryPojo save(CategoryPojo categoryPojo) {
		long parentId = categoryPojo.getParentId();
		if (parentId != 0) {
			CategoryPojo categoryPojoParent = categoryRepository.findByCategoryId(parentId);
			List<CategoryPojo> childrens = categoryPojoParent.getChildCategories();
			categoryPojo.setParent(categoryPojoParent);
			childrens.add(categoryPojo);
			categoryPojoParent.setChildCategories(childrens);
			CategoryPojo categoryPojo2 = categoryRepository.save(categoryPojo);
			return categoryPojo2;
		}
		CategoryPojo categoryPojo2 = categoryRepository.save(categoryPojo);
		return categoryPojo2;
	}
	
	public List<CategoryPojo> findAll() {
		//List<CategoryPojo> categoryPojos = categoryRepository.findAll();
		List<CategoryPojo> root = categoryRepository.findByParent(null);
		List<Long> categoryIds = new ArrayList<>();
		for (CategoryPojo categoryPojo : root) {
			categoryIds.add(categoryPojo.getCategoryId());
		}
		List<CategoryPojo> categoryTree = categoryRepository.findByCategoryIdIn(categoryIds);
		return categoryTree;
	}
}
