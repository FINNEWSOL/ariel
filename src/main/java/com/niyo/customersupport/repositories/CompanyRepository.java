package com.niyo.customersupport.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.CompanyPojo;

public interface CompanyRepository extends JpaRepository<CompanyPojo, Serializable>{

	public CompanyPojo save(CompanyPojo companyPojo);
	
	public CompanyPojo findByCorporateId(String corporateId);
}