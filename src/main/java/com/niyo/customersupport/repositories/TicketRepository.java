package com.niyo.customersupport.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.TicketPojo;

public interface TicketRepository extends JpaRepository<TicketPojo, Serializable>{
	
	public TicketPojo save(TicketPojo ticketPojo);

	public TicketPojo findFirstByTicketId(long ticketId);
	
	public TicketPojo findFirstByFreshdeskTicketId(Integer freshDeskTicketId);
	
	public List<TicketPojo> findByCrn(String crn);
	
	public Page<TicketPojo> findByCrn(String crn, Pageable page);
	
	public Page<TicketPojo> findAll(Pageable page);
	
	public long countCrnByCrn(String crn);
}