package com.niyo.customersupport.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.CategoryPojo;

public interface CategoryRepository extends JpaRepository<CategoryPojo, Serializable>{

	public CategoryPojo save(CategoryPojo categoryPojo);
	
	public List<CategoryPojo> findAll();
	
	public CategoryPojo findByCategoryId(long categoryId);
	
	public List<CategoryPojo> findByParent(CategoryPojo categoryPojo);
	
	public List<CategoryPojo> findByCategoryIdIn(List<Long> categoryIds);
}
