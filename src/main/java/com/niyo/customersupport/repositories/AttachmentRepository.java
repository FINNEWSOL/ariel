package com.niyo.customersupport.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.AttachmentPojo;

public interface AttachmentRepository extends JpaRepository<AttachmentPojo, Serializable>{

	
	public AttachmentPojo save(AttachmentPojo attachmentPojo);
}
