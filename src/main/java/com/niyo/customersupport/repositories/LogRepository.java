package com.niyo.customersupport.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.LogPojo;

public interface LogRepository extends JpaRepository<LogPojo, Serializable> {
	
	public LogPojo save(LogPojo logPojo);

}
