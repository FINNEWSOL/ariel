package com.niyo.customersupport.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;

public interface ConversationRepository extends JpaRepository<ConversationPojo, Serializable>{

	public ConversationPojo save(ConversationPojo conversationPojo);
	
	public ConversationPojo findByFreshDeskConversationId(long freshDeskConversationId);
}
