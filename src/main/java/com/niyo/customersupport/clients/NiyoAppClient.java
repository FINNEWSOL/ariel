package com.niyo.customersupport.clients;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.config.niyoapp.Balance;
import com.niyo.customersupport.config.niyoapp.Card;
import com.niyo.customersupport.config.niyoapp.Credentials;
import com.niyo.customersupport.config.niyoapp.Customer;
import com.niyo.customersupport.services.StaticResourceService;
import com.niyo.customersupport.utils.restutils.RestUtil;

@Service
public class NiyoAppClient {
	
	@Autowired
	private Customer customer;
	
	@Autowired
	private RestUtil restUtil;
	
	@Autowired
	private Credentials credentials;
	
	@Autowired
	private Card card;
	
	@Autowired
	private Balance balance;
	
	
	public JsonNode getCustomerDetailByParameter(String parameter, String value) throws UnirestException {
		JsonNode customerDetails = restUtil.get(customer.getGet() + "?" + parameter + "=" + value, getHeaders(StaticResourceService.TOKEN));
		return customerDetails;
	}
	
	public JsonNode addFreshdeskIdIntoCustomer(String crn, long freshDeskId) throws UnirestException, JSONException {
		JSONObject request = new JSONObject();
		request.put("freshDeskCustId", freshDeskId);
		JsonNode response = restUtil.patch(customer.getUpdate() + "/" + crn + "/external-identifiers", request.toString(), getHeaders(StaticResourceService.TOKEN));
		return response;
	}
	
	public Map<String,String> getHeaders(String token) {
		Map<String,String> headers = new HashMap<>();
		headers.put("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.put("x-auth-token", token);
		return headers;
	}
	
	public String getIdentificationId(String crn) throws UnirestException, JSONException {
		Map<String,String> headers = new HashMap<>();
		headers.put("x-auth-token", StaticResourceService.TOKEN);
		JsonNode cognitoDetails = restUtil.get(customer.getIdentificationkey() + crn, headers);
		String identityId = cognitoDetails.getObject().getString("identityId");
		return identityId;
	}

	public String getToken() throws UnirestException, JSONException {
		JSONObject request = new JSONObject();
		request.put("key", credentials.getKey());
		Headers responseHeaders = restUtil.postGetHeader(credentials.getLogin(), request.toString());
		String token = responseHeaders.getFirst("x-auth-token");
		return token;
	}
	
	public Boolean blockCard(Map<String,Object> allRequestParams) {
		String url = card.getBlockcard();
		try {
			HttpResponse<JsonNode> response = restUtil.getWithQueryParams(url, allRequestParams);
			if (response.getStatus() == 200) {
				return true;
			}
		} catch (UnirestException e) {
			return false;
		}
		return false;
	}
	
	public Boolean checkBalance(Map<String,Object> allRequestParams) {
		String url = balance.getCheckbalance();
		try {
			HttpResponse<JsonNode> response = restUtil.getWithQueryParams(url, allRequestParams);
			if (response.getStatus() == 200) {
				return true;
			}
		} catch (UnirestException e) {
			return false;
		}
		return false;
	}
}
