package com.niyo.customersupport.clients;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.config.freshdesk.Agent;
import com.niyo.customersupport.config.freshdesk.Company;
import com.niyo.customersupport.config.freshdesk.Credentials;
import com.niyo.customersupport.config.freshdesk.Customer;
import com.niyo.customersupport.config.freshdesk.Tickets;
import com.niyo.customersupport.pojos.CompanyPojo;
import com.niyo.customersupport.pojos.FreshDeskTicketPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.utils.restutils.RestUtil;

@Service
public class FreshDeskClient {
	
	@Autowired
	private RestUtil restUtil;
	
	@Autowired
	private Credentials credentials;
	
	@Autowired
	private Tickets tickets;
	
	@Autowired
	private Customer customer;
	
	@Autowired
	private Agent agent;
	
	@Autowired
	private Company company;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	public JsonNode createTicketInFreshdesk(FreshDeskTicketPojo ticketPojo) throws UnirestException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String request = mapper.writeValueAsString(ticketPojo);
		JsonNode response = restUtil.post(tickets.getCreate(), request, credentials.getUsername(), credentials.getPassword());
		return response;
	}
	
	public JsonNode createTicketInFreshDeskWithAttachment(Map<String,Object> body, List<File> files) throws JsonProcessingException, UnirestException {
		JsonNode response = restUtil.multipartPost(tickets.getCreate(), body, credentials.getUsername(), credentials.getPassword(),files);
		return response;
	}
	
	public JsonNode getRequeterInfoFromFreshDeskByRequetserId(long id) throws UnirestException {
		JsonNode freshdeskRequesterDetail = restUtil.get(customer.getGet() + id, credentials.getUsername(), credentials.getPassword());
		return freshdeskRequesterDetail;
	}
	
	public JsonNode getTicketDetaisInFreshDesk(Integer id) throws UnirestException {
		JsonNode ticketDetails = restUtil.get(tickets.getGet() + id, credentials.getUsername(), credentials.getPassword());
		return ticketDetails;
	}
	
	public JsonNode replyToTicket(Integer freshDeskTicketId, Map<String,Object> body, List<File> files) throws UnirestException {
		String url = tickets.getReply();
		String finalUrl = StringUtils.replace(url, "{ticketId}", freshDeskTicketId.toString());
		JsonNode response = restUtil.multipartPost(finalUrl, body, credentials.getUsername(), credentials.getPassword(),files);
		return response;
	}
	
	public File downloadAttachmentFromFreshDesk(String url, String fileName) throws UnirestException, IOException {
		File file = new File(System.getProperty("java.io.tmpdir") + File.separator + fileName);
		FileUtils.copyURLToFile(new URL(url), file);
		return file;
	}
	
	public JsonNode getConversationsOfTicket(Integer freshDeskTicketId, int page) throws UnirestException {
		String url = StringUtils.replace(tickets.getConversation(), "{ticketId}", freshDeskTicketId.toString());
		String finalUrl = url + "?" + "page=" + page;
		JsonNode response = restUtil.get(finalUrl, credentials.getUsername(), credentials.getPassword());
		return response;
	}
	
	public JsonNode updateCustomerInfo(long requesterId, Map<String,Object> body) throws UnirestException {
		String url = customer.getUpdate();
		String finalUrl = url + requesterId;
		JsonNode response = restUtil.put(finalUrl, body, credentials.getUsername(), credentials.getPassword());
		return response;
	}
	
	public JsonNode getAgentDetails() throws UnirestException {
		JsonNode response = restUtil.get(agent.getGet(), credentials.getUsername(), credentials.getPassword());
		return response;
	}
	
	public CompanyPojo createCorporate(CompanyPojo companyPojo) throws UnirestException, JsonProcessingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String request = mapper.writeValueAsString(companyPojo);
		HttpResponse<JsonNode> response = Unirest.post(company.getCreate()).header("content-type", MediaType.APPLICATION_JSON_VALUE).basicAuth(credentials.getUsername(), credentials.getPassword()).body(request).asJson();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		CompanyPojo companyPojo2 = mapper.readValue(response.getBody().toString(), CompanyPojo.class);
		return companyPojo2;
	}
	
	public FreshDeskTicketPojo updateTicket(TicketPojo ticketPojo, Integer ticketId) throws UnirestException, IOException {
		FreshDeskTicketPojo freshDeskTicketPojo = dozerBeanMapper.map(ticketPojo, FreshDeskTicketPojo.class);
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> request = mapper.convertValue(freshDeskTicketPojo, Map.class);
		String url = tickets.getUpdate() + "/" + ticketId;
		HttpResponse<JsonNode> response = Unirest.put(url).basicAuth(credentials.getUsername(), credentials.getPassword()).header("content-type", MediaType.MULTIPART_FORM_DATA_VALUE).fields(request).asJson();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		FreshDeskTicketPojo freshDeskTicketPojo2 = mapper.readValue(response.getBody().toString(), FreshDeskTicketPojo.class);
		return freshDeskTicketPojo2;
	}
}
