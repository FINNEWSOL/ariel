package com.niyo.customersupport.clients;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.niyo.customersupport.config.aws.Credentials;
import com.niyo.customersupport.utils.restutils.RestUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Spring java configuration class for AmazonWS.
 * @author nshelton
 */

@Configuration
@Slf4j
public class AWSClient {

	/**
     * Creates an Amazon S3 Client.
     * @return AmazonS3Client
     */
//	@Autowired
//	private Credentials credentials;
	
    @Bean
    public AmazonS3 amazonS3Client() throws IOException {
    	
    	//BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(credentials.getAccesskey(), credentials.getSecretkey());
//    	AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
//                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//                .build();
    	AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new InstanceProfileCredentialsProvider(true))
                .build();
    	log.info("Initializing s3Client");
    	org.junit.Assert.assertNotNull("s3Client Not Null", s3Client);
        return s3Client;
    }
    
    @Bean
    public TransferManager getTransferManager() throws IOException {
    	TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(amazonS3Client()).build();
    	org.junit.Assert.assertNotNull("Transfer manager Not Null", transferManager);
    	log.info("Initializing Transfer manager");
    	return transferManager;
    }
}
