package com.niyo.customersupport.adapter;

import java.util.ArrayList;
import java.util.List;

import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;

public class TicketAdapter {

	public static TicketPojo removePrivateNotes(TicketPojo ticketPojo) {
		List<ConversationPojo> finalConversationPojos = new ArrayList<ConversationPojo>();
		List<ConversationPojo> conversationPojos = ticketPojo.getConversations();
		if (conversationPojos != null && conversationPojos.size() > 0) {
			for (ConversationPojo conversationPojo : conversationPojos) {
				if (!conversationPojo.isPrivateNote()) {
					finalConversationPojos.add(conversationPojo);
				}
			}
			ticketPojo.setConversations(finalConversationPojos);
		}
		return ticketPojo;
	}

	public static List<TicketPojo> removePrivateNotes(List<TicketPojo> ticketPojos) {
		List<TicketPojo> finalTicketPojos = new ArrayList<>();
		for (TicketPojo ticketPojo : ticketPojos) {
			TicketPojo finalTicketPojo = removePrivateNotes(ticketPojo);
			finalTicketPojos.add(finalTicketPojo);
		}
		return finalTicketPojos;
	}
}
