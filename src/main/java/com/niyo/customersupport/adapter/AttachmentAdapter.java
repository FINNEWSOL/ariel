package com.niyo.customersupport.adapter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.pojos.AttachmentPojo;
import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.services.TicketService;

@Component
public class AttachmentAdapter {
	
	@Autowired
	TicketService ticketService;
	
	public List<AttachmentPojo> getAttachmentPojoFromFiles(List<File> files, TicketPojo ticketPojo) throws UnirestException, JSONException, IOException {
		List<AttachmentPojo> attachmentPojos = new ArrayList<>();
		for (File file : files) {
			String uploadPath = ticketService.uploadTicketFilesToS3(ticketPojo.getCrn(), file);
			AttachmentPojo attachmentPojo = new AttachmentPojo();
			attachmentPojo.setContentType(Files.probeContentType(file.toPath()));
			attachmentPojo.setKey(uploadPath);
			attachmentPojo.setFileName(file.getName());
			attachmentPojo.setTicketId(ticketPojo);
			attachmentPojos.add(attachmentPojo);
		}
		return attachmentPojos;
	}
	
	public List<AttachmentPojo> getAttachmentPojoFromFiles(List<File> files, TicketPojo ticketPojo, ConversationPojo conversationPojo) throws UnirestException, JSONException, IOException {
		List<AttachmentPojo> attachmentPojos = new ArrayList<>();
		for (File file : files) {
			String uploadPath = ticketService.uploadTicketFilesToS3(ticketPojo.getCrn(), file);
			AttachmentPojo attachmentPojo = new AttachmentPojo();
			attachmentPojo.setContentType(Files.probeContentType(file.toPath()));
			attachmentPojo.setKey(uploadPath);
			attachmentPojo.setFileName(file.getName());
			attachmentPojo.setConversationId(conversationPojo);
			attachmentPojos.add(attachmentPojo);
		}
		return attachmentPojos;
	}

}
