package com.niyo.customersupport.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.pojos.CategoryPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.security.services.NiyoUserDetailsService;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;
import com.niyo.customersupport.services.CategoryService;

@RestController
@RequestMapping(value = "/api/v1/categories")
public class CategoryController {
	
	@Autowired
	private NiyoUserDetailsService niyoUserDetailsService;
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = "", method=RequestMethod.POST)
	public ResponseEntity<?> createCategory(@RequestHeader("api-key") String key,
			@RequestHeader("api-secret") String secret,
			@RequestBody CategoryPojo categoryPojo) throws UnirestException, IOException {
		try {
			niyoUserDetailsService.loadArielUserDetails(key, secret);
		} catch (NiyopayAuthenticationException e) {
			return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
		}
		CategoryPojo categoryPojo2 = categoryService.save(categoryPojo);
		return new ResponseEntity<CategoryPojo>(categoryPojo2, HttpStatus.OK);
	}
	
	@RequestMapping(value = "", method=RequestMethod.GET)
	public ResponseEntity<?> getCategories() throws UnirestException, IOException {
		List<CategoryPojo> categoryPojos = categoryService.findAll();
		return new ResponseEntity<List<CategoryPojo>>(categoryPojos, HttpStatus.OK);
	}

}
