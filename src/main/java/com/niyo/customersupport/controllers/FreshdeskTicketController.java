package com.niyo.customersupport.controllers;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.pojos.UpdateStatusRequestPojo;
import com.niyo.customersupport.security.services.NiyoUserDetailsService;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;
import com.niyo.customersupport.services.ConversationService;
import com.niyo.customersupport.services.TicketService;
import com.niyo.customersupport.utils.restutils.exceptions.ReplyAlreadyExistsException;
import com.niyo.customersupport.utils.restutils.exceptions.TicketAlreadyExistsException;
import com.niyo.customersupport.utils.restutils.exceptions.TicketNotFoundException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/api/freshdesk/v1/tickets")
public class FreshdeskTicketController {
	
	@Autowired
	private ConversationService conversationService;
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private NiyoUserDetailsService niyoUserDetailsService;
	
	@RequestMapping(value = "/event/mail", method=RequestMethod.POST)
	public ResponseEntity<?> freshDeskEventTicketCreation(@RequestHeader("api-key") String key,
			@RequestHeader("api-secret") String secret,
			@RequestBody TicketPojo ticketPojo) throws UnirestException, IOException, JSONException {
		try {
			niyoUserDetailsService.loadArielUserDetails(key, secret);
		} catch (NiyopayAuthenticationException e) {
			return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
		}
		TicketPojo ticketPojo2;
		try {
			ticketPojo2 = ticketService.createTicket(ticketPojo);
		} catch (TicketAlreadyExistsException e) {
			log.error("This ticket is already present in niyo customer support system no need to create duplicate");
			return ResponseEntity.ok().header("error", "This ticket is already present in niyo customer support system no need to create duplicate one.").body(null);
		}
		return new ResponseEntity<TicketPojo>(ticketPojo2, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update_status/{freshDeskTicketId}", method=RequestMethod.PUT)
	public ResponseEntity<?> updateStatus(@RequestHeader("api-key") String key,
			@RequestHeader("api-secret") String secret,
			@PathVariable Integer freshDeskTicketId, 
			@RequestBody UpdateStatusRequestPojo updateStatusRequestPojo) {
		TicketPojo ticketPojo=null;
		try {
			niyoUserDetailsService.loadArielUserDetails(key, secret);
			ticketPojo = ticketService.updateStatus(freshDeskTicketId, updateStatusRequestPojo);
		} catch (TicketNotFoundException e) {
			log.error("Ticket is not present in niyo customer support system");
			return ResponseEntity.ok().header("error", "This ticket is not present in niyo customer support system").body(null);
		} catch (NiyopayAuthenticationException e) {
			log.error("Authentication failed");
			return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<TicketPojo>(ticketPojo, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update_priority/{freshDeskTicketId}", method=RequestMethod.PUT)
	public ResponseEntity<?> updatePriority(@RequestHeader("api-key") String key,
			@RequestHeader("api-secret") String secret,
			@PathVariable Integer freshDeskTicketId, 
			@RequestBody UpdateStatusRequestPojo updateStatusRequestPojo) {
		TicketPojo ticketPojo=null;
		try {
			niyoUserDetailsService.loadArielUserDetails(key, secret);
			ticketPojo = ticketService.updatePriority(freshDeskTicketId, updateStatusRequestPojo);
		} catch (TicketNotFoundException e) {
			log.error("Ticket not found in niyo customer support system");
			return ResponseEntity.ok().header("error", "This ticket is not present in niyo customer support system").body(null);
		} catch (NiyopayAuthenticationException e) {
			log.error("Authentication failed");
			return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<TicketPojo>(ticketPojo, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/freshdeskevent/{freshDeskTicketId}", method = RequestMethod.POST)
	public ResponseEntity<?> replyFromMail(@RequestHeader("api-key") String key,
			@RequestHeader("api-secret") String secret,
			@PathVariable Integer freshDeskTicketId) throws UnirestException, JSONException, IOException {
		ConversationPojo conversationPojo = null;
			try {
				niyoUserDetailsService.loadArielUserDetails(key, secret);
				conversationPojo = conversationService.replyToTicket(freshDeskTicketId);
			} catch (TicketNotFoundException e) {
				log.error("Ticket is not present in niyo customer support system");
				return ResponseEntity.ok().header("error", "This ticket is not present in niyo customer support system").body(null);
			} catch (ReplyAlreadyExistsException e) {
				log.error("Reply already exists in system no need to create duplicate");
				return ResponseEntity.ok().header("error", "This reply is already present in niyo customer support system").body(null);
			} catch (NiyopayAuthenticationException e) {
				return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
			}
		return new ResponseEntity<ConversationPojo>(conversationPojo, HttpStatus.OK);
	}
}
