package com.niyo.customersupport.controllers;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.NiyoAppClient;
import com.niyo.customersupport.exceptions.InvalidMobileNumberException;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.security.services.NiyoUserDetailsService;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;
import com.niyo.customersupport.services.TicketService;

import lombok.extern.slf4j.Slf4j;



@Slf4j
@RestController
@RequestMapping(value = "/api/exotel/v1/tickets")
public class ExotelTicketController {
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private NiyoUserDetailsService niyoUserDetailsService;
	
	@RequestMapping(value = "/event/exotel/{flowId}/{partnerBank}/{apikey}", method=RequestMethod.GET)
	public ResponseEntity<?> createTicketFromExotel(
			@PathVariable String flowId,
			@PathVariable String apikey,
			@PathVariable String partnerBank,
			@RequestParam(value = "From", required=false) String callReceivedFrom,
			@RequestParam(value = "RecordingUrl", required=false) String url,
			@RequestParam(value = "digits", required=false) String registeredMobileNumber,
			@RequestParam(value = "auth", required = false) String authKey,
            @RequestParam(value = "CallSid", required = false) String callSid,
            @RequestParam(value = "To", required = false) String to,
            @RequestParam(value = "Direction", required = false) String direction,
            @RequestParam(value = "DialCallDuration", required = false) String dialCallDuration,
            @RequestParam(value = "StartTime", required = false) String startTime,
            @RequestParam(value = "EndTime", required = false) String endTime,
            @RequestParam(value = "CallType", required = false) String callType,
            @RequestParam(value = "DialWhomNumber", required = false) String dialWhomNumber,
            @RequestParam(value = "Created", required = false) String created,
            @RequestParam(value = "flow_id", required = false) String flow_id,
            @RequestParam(value = "tenant_id", required = false) String tenant_id,
            @RequestParam(value = "CallFrom", required = false) String callFrom,
            @RequestParam(value = "CallTo", required = false) String callTo,
            @RequestParam(value = "ForwardedFrom", required = false) String forwardedFrom,
            @RequestParam(value = "CurrentTime", required = false) String currentTime,
            @RequestParam Map<String,Object> allRequestParams) {
		TicketPojo ticketPojo=null;
			try {
				niyoUserDetailsService.loadArielUserDetails(apikey, null);
			} catch (NiyopayAuthenticationException e) {
				log.error("Authentication failed");
				return new ResponseEntity<TicketPojo>(HttpStatus.UNAUTHORIZED);
			}
			try {
				ticketPojo = ticketService.createTicketFromExotel(url, registeredMobileNumber, flowId, callReceivedFrom, allRequestParams, partnerBank);
			} catch (IllegalStateException e) {
				e.printStackTrace();
				return new ResponseEntity(HttpStatus.BAD_REQUEST);
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseEntity(HttpStatus.BAD_REQUEST);
			} catch (UnirestException e) {
				e.printStackTrace();
				return new ResponseEntity(HttpStatus.BAD_REQUEST);
			} catch (InvalidMobileNumberException e) {
				return new ResponseEntity(HttpStatus.OK);
			}
		 
		return new ResponseEntity<TicketPojo>(ticketPojo, HttpStatus.OK);
	}

}
