package com.niyo.customersupport.controllers;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.TicketPojo;
import com.niyo.customersupport.services.ConversationService;
import com.niyo.customersupport.services.TicketService;

@RestController
@RequestMapping(value = "/api/app/v1/tickets")
public class AppTicketController {
	
	@Autowired
	private TicketService ticketService;
	
	@Autowired
	private ConversationService conversationService;
	
	@RequestMapping(value = "/{crn}", method=RequestMethod.POST)
	public ResponseEntity<?> createTicket(@PathVariable String crn,
			@RequestBody TicketPojo ticketPojo) throws UnirestException, IOException {
		TicketPojo ticketPojo2 = null;
		try {
			ticketPojo2 = ticketService.createTicketInFreshDesk(ticketPojo);
		} catch (JSONException e) {
			return new ResponseEntity<TicketPojo>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<TicketPojo>(ticketPojo2, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/{crn}/{ticketId}", method = RequestMethod.GET)
	public ResponseEntity<TicketPojo> fetchById(@PathVariable String crn,
			@PathVariable long ticketId) {
		TicketPojo ticketPojo = ticketService.getTicketDetailsByTicketId(ticketId);
		if (ticketPojo == null) {
			return new ResponseEntity<TicketPojo>(ticketPojo, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<TicketPojo>(ticketPojo, HttpStatus.OK);
	}

	@RequestMapping(value = "/{crn}", method = RequestMethod.GET)
	public ResponseEntity<List<TicketPojo>> fetchByIdPagable(
			@PathVariable String crn,
			Pageable page) {
		List<TicketPojo> ticketPojos = ticketService.getTicketsByCrn(crn,page);
		if (ticketPojos.isEmpty()) {
			return new ResponseEntity<List<TicketPojo>>(ticketPojos, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<TicketPojo>>(ticketPojos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reopen/{ticketId}", method=RequestMethod.POST)
	public ResponseEntity<?> reopenTicket(@PathVariable long ticketId, @RequestBody ConversationPojo conversationPojo) throws UnirestException, IOException, JSONException {
		ticketService.reopenTicket(ticketId, conversationPojo);
		return new ResponseEntity<String>("Ticket reopened", HttpStatus.OK);
	}
	
	@RequestMapping(value = "{crn}/{ticketId}/reply", method = RequestMethod.POST)
	public ResponseEntity<?> replyInTicket(
			@PathVariable String crn,
			@PathVariable long ticketId, 
			@RequestBody ConversationPojo conversationPojo) throws JsonParseException, 
	JsonMappingException, UnirestException, IOException {
		ConversationPojo conversationPojo2 = conversationService.replyToTicket(ticketId, conversationPojo);
		return new ResponseEntity<ConversationPojo>(conversationPojo2,HttpStatus.CREATED);
	}
}
