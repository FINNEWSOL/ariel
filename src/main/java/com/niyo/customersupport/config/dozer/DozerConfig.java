package com.niyo.customersupport.config.dozer;

import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.niyo.customersupport.pojos.ConversationPojo;
import com.niyo.customersupport.pojos.FreshDeskConversationPojo;
import com.niyo.customersupport.pojos.FreshDeskTicketPojo;
import com.niyo.customersupport.pojos.TicketPojo;

@Configuration
public class DozerConfig {
	
	@Bean
	public DozerBeanMapper mapper() throws Exception {
	    DozerBeanMapper mapper = new DozerBeanMapper();
	    mapper.addMapping(objectMappingBuilder);
	    return mapper;
	}

	BeanMappingBuilder objectMappingBuilder = new BeanMappingBuilder() {
	    @Override
	    protected void configure() {
	    	mapping(TicketPojo.class, FreshDeskTicketPojo.class).fields("crn", "customFields");
	    	mapping(ConversationPojo.class, FreshDeskConversationPojo.class).fields("repliersFreshDeskId", "userId");
	    }
	};
}
