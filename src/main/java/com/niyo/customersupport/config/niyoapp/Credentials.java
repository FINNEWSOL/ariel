package com.niyo.customersupport.config.niyoapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component("niyocredentials")
@Data
@ConfigurationProperties(prefix = "spring.niyoapp.creds")
public class Credentials {
	
	private String key;
	
	private String token;
	
	private String login;
	
}
