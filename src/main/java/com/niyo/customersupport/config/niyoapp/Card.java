package com.niyo.customersupport.config.niyoapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "spring.niyoapp.card")
public class Card {

	private String blockcard;
}
