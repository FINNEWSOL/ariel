package com.niyo.customersupport.config.niyoapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.niyo.customersupport.config.freshdesk.Tickets;

import lombok.Data;

@Component("niyocustomer")
@Data
@ConfigurationProperties(prefix = "spring.niyoapp.customer")
public class Customer {

	private String get;
	
	private String update;
	
	private String identificationkey;
}
