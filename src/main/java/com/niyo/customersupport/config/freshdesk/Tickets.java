package com.niyo.customersupport.config.freshdesk;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "spring.freshdesk.ticket")
public class Tickets {

	private String create;
	
	private String update;
	
	private String get;
	
	private String reply;
	
	private String conversation;
}
