package com.niyo.customersupport.config.freshdesk;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Component("freshdeskcredentials")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "spring.freshdesk.creds")
public class Credentials {
	
	@NonNull
	@NotBlank
	private String username;
	
	@NonNull
	private String password;

}
