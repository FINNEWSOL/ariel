package com.niyo.customersupport.config.freshdesk;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component("freshdeskcustomer")
@Data
@ConfigurationProperties(prefix = "spring.freshdesk.customer")
public class Customer {
	
	private String get;
	
	private String update;

}
