package com.niyo.customersupport.config.freshdesk;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "spring.freshdesk.agent")
public class Agent {
	
	private String get;

}
