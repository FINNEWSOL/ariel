package com.niyo.customersupport.config.freshdesk;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "spring.freshdesk.company")
public class Company {

	private String create;
	
	private String get;
}
