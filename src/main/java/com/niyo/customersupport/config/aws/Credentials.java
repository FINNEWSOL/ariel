package com.niyo.customersupport.config.aws;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component("awscredentials")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "spring.aws.creds")
public class Credentials {

	private String accesskey;
	
	private String secretkey;
	
	private String bucketname;
}
