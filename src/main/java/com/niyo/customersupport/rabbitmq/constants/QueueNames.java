package com.niyo.customersupport.rabbitmq.constants;

public class QueueNames {
	
	public static final String TRANSACTION_FAILED = "transaction-declined-ariel";
	
	public static final String TRANSFER_FAILED = "fund-transfer-failed-ariel";

}
