package com.niyo.customersupport.rabbitmq.service;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.exceptions.CorporateCreationFailedException;
import com.niyo.customersupport.pojos.CardTransaction;
import com.niyo.customersupport.pojos.FundTransfer;
import com.niyo.customersupport.rabbitmq.constants.QueueNames;
import com.niyo.customersupport.services.IncidentsService;
import com.rabbitmq.client.Channel;

@Component
public class RabbitConsumerService {
	
	@Autowired
	private IncidentsService IncidentsService;
	
	@RabbitListener(queues = QueueNames.TRANSACTION_FAILED)
	public void consumeTransactionFailedEvents(@Payload String data, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws JsonParseException, JsonMappingException, IOException, UnirestException, CorporateCreationFailedException, JSONException {
		channel.basicAck(tag, false);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		CardTransaction cardTransaction = mapper.readValue(data, CardTransaction.class);
		IncidentsService.createIncidenceForTransactionFailure(cardTransaction, channel, tag);
	}
	
	@RabbitListener(queues = QueueNames.TRANSFER_FAILED)
	public void consumeTrasferFailedEvents(@Payload String data, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws JsonParseException, JsonMappingException, IOException, UnirestException, CorporateCreationFailedException, JSONException {
		channel.basicAck(tag, false);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		FundTransfer fundTransfer = mapper.readValue(data, FundTransfer.class);
		IncidentsService.createIncidenceForTransferFailure(fundTransfer, channel, tag);
	}

}
