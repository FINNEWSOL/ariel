package com.niyo.customersupport.pojos;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Entity
@Data
@Table(name = "attachment")
@JsonInclude(Include.NON_NULL)
public class AttachmentPojo {

	@Id
	@GeneratedValue//(strategy = GenerationType.IDENTITY)
	private long attachmentId;
	
	@ManyToOne
	@Getter(AccessLevel.NONE)
	@JoinColumn(name="ticket_id")
	private TicketPojo ticketId;
	
	@ManyToOne
	@Getter(AccessLevel.NONE)
	@JoinColumn(name="conversation_id")
	private ConversationPojo conversationId;
	
	private String contentType;
	
	private String fileName;
	
	@Column(name="url")
	private String key;
	
	@CreationTimestamp
	private Timestamp createdTime;

}
