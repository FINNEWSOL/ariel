package com.niyo.customersupport.pojos;

import java.util.Map;

import lombok.Data;

@Data
public class NiyoCustomerPojo {
	
	private String crn;
	
	private String name;
	
	private String firstName;
	
	private String nickName;
	
	private Map<String,String> email;
	
	private Map<String,String> phone;
	
	private Map<String,String> currentEmployer;
	
	private Map<String,String> niyoAccount;
	
	public boolean isValid() {
	    return crn != null || name != null || firstName != null || nickName != null || 
	    		email != null || phone != null || currentEmployer != null || niyoAccount != null;
	  }

}
