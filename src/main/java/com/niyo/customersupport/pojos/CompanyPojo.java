package com.niyo.customersupport.pojos;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "company")
public class CompanyPojo {
	
	@Id
	private Long id;

	@JsonProperty("name")
	private String corporateId;
	
	private String description;
}
