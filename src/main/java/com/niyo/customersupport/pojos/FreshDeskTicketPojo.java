package com.niyo.customersupport.pojos;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class FreshDeskTicketPojo {

	@JsonProperty("[custom_fields][crn]")
	private String customFields;
	
	@JsonProperty("[custom_fields][transactionid]")
	private String transactionId;
	
//	@JsonProperty("[custom_fields][transactiontype]")
//	private String transactionType;
	
	@JsonProperty("[custom_fields][categories]")
	private String category;
	
	@JsonProperty("[custom_fields][corporateid]")
	private String corporateid;
	
	@Transient
	private String email;
	
	@Transient
	private String phone;
	
	@Transient
	private String name;
	
	private String subject;
	
	private String description;
	
	private Integer status;
	
	private Integer priority;
	
	private Integer source;
	
	private String type;
	
	@JsonProperty("[custom_fields][partner_bank]")
	private String partnerBank;
	
	private Integer id;
	
	@JsonProperty("requester_id")
	private Long freshDeskRequesterId;
	
	@JsonProperty("group_id")
	private Integer groupId;
	
}
