package com.niyo.customersupport.pojos;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class FreshDeskConversationPojo {

	private String body;
	
	@Column(name="from_email")
	@JsonProperty("from_email")
	private String fromEmail;
	
	@Column(name="user_id")
	@JsonProperty("user_id")
	private long userId;
}
