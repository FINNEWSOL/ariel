package com.niyo.customersupport.pojos;

import lombok.Data;

@Data
public class AgentPojo {

	private Long id;
	
	private Contact contact;
}
