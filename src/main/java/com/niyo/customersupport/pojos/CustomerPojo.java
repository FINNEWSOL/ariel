package com.niyo.customersupport.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class CustomerPojo {
	
	private Number id;
	
	private String name;
	
	private String phone;
	
	@JsonProperty("[custom_fields][crn]")
	private String crn;
	
	@JsonProperty("[custom_fields][corporateid]")
	private String corporateid;
	
	private String email;
	
	@JsonProperty("company_id")
	private Number companyId;
}
