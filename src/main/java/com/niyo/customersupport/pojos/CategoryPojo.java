package com.niyo.customersupport.pojos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Entity
@Data
@Table(name="category")
@JsonInclude(Include.NON_NULL)
public class CategoryPojo {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="category_id")
	private long categoryId;
	
	private String category;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@Getter(AccessLevel.NONE)
	private CategoryPojo parent;
	
	@OneToMany(mappedBy="parent")
	private List<CategoryPojo> childCategories;
	
	@Transient
	@JsonProperty(access = Access.WRITE_ONLY)
	private long parentId;
}
