package com.niyo.customersupport.pojos;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Entity
@Data
@Table(name="tickets")
@JsonInclude(Include.NON_NULL)
public class TicketPojo {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ticket_id")
	private long ticketId;
	
	private String crn;
	
	private String transactionId;
	
	private String transactionType;
	
	@JsonProperty("externalTicketId")
	private Integer freshdeskTicketId;
	
	private Long freshDeskRequesterId;
	
	private String category;
	
	@Transient
	private String email;
	
	@Transient
	private String phone;
	
	@Transient
	private String name;
	
	@Column(length = 1000)
	private String subject;
	
	@Column(length = 3000)
	private String description;
	
	private Integer status;
	
	private Integer priority;
	
	private Integer source;
	
	private Integer groupId;
	
	private String corporateid;
	
	private String type;
	
	private String partnerBank;
	
	@OneToMany(mappedBy="ticketId",cascade = {CascadeType.ALL})
	private List<AttachmentPojo> attachments;
	
	@OneToMany(mappedBy="ticketId",cascade = {CascadeType.ALL})
	private List<ConversationPojo> conversations;
	
	@CreationTimestamp
	private Timestamp createdTime;
	
	@UpdateTimestamp
	private Timestamp lastModifiedTime;
	
	@Transient
	@JsonIgnore
	private String corporateName;
	
}
