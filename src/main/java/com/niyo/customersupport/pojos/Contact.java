package com.niyo.customersupport.pojos;

import lombok.Data;

@Data
public class Contact {
	
	private boolean active;

	private String name;
	
	private String phone;
}
