package com.niyo.customersupport.pojos;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Entity
@Data
@Table(name = "conversation")
public class ConversationPojo {

	@Id
	@GeneratedValue
	@Column(name="conversation_id")
	private long conversationId;
	
	@ManyToOne
	@Getter(AccessLevel.NONE)
	@JoinColumn(name="ticket_id")
	private TicketPojo ticketId;
	
	private long freshDeskConversationId;
	
	@Column(length = 3000)
	private String body;
	
	@Column(name="from_email")
	private String fromEmail;
	
	private boolean isAgent;
	
	private boolean isPrivateNote;
		
	private long repliersFreshDeskId;
	
	private String repliersName;
	
	@OneToMany(mappedBy="conversationId",cascade = {CascadeType.ALL})
	private List<AttachmentPojo> attachments;
	
	@CreationTimestamp
	private Timestamp createdTime;
	
}
