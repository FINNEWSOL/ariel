package com.niyo.customersupport.pojos;

import com.niyo.customersupport.enums.TransactionType;

import lombok.Data;

@Data
public class CardTransaction {
	
	private String id;
	
	private String crn;
	
	private String corporateCode;
	
	private String merchantCategory;
	
	private String txn_amount;
	
	private String description;
	
	private String billing_amount;
	
	private String mcc_code;
	
	private String merchant_id;
	
	private String merchant_name;
	
	private String response_code;
	
	private String response_reason;
	
	private String productCode;
	
	private String transactionTimeStamp;
	
	private String pocket;
	
	private String responseDescription;
	
	private String billedPocketBalance;
	
	private String cardBalance;
	
	private TransactionType transactionType;

}
