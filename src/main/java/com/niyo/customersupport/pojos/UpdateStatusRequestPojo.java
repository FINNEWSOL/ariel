package com.niyo.customersupport.pojos;

import lombok.Data;

@Data
public class UpdateStatusRequestPojo {
	
	String status;
	
	String source;
	
	String priority;

}
