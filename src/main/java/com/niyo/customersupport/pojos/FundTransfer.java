package com.niyo.customersupport.pojos;

import lombok.Data;

@Data
public class FundTransfer {
	
	private String fundTransferId;
	
	private String crn;
	
	private String status;
	
	private String refID;
	
	private String beneficiaryId;
	
	private String networkType;

}
