package com.niyo.customersupport.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TicketType {

	QUERY("Query"), 
	INCIDENT("Incident"), 
	PROBLEM("Problem"),
	FEATURE_REQ("Feature Request"),
	IVR("IVR"),
	TRANSACTION_DECLINED("DeclineTxn"),
	TRANSFER_FAILED("Transfer Failed");
	
	private final String value;
	
}
