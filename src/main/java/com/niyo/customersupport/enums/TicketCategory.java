package com.niyo.customersupport.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TicketCategory {
	
	INSUFFICIENT_BALANCE("Insufficient Balance"),
	CARD_LOST("Card lost / Replacement"),
	INCORRECT_MCC("Incorrect MCC"),
	PERSONAL_DATA_UPDATE("Personal Data Update"),
	REFUNDS("Refunds"),
	DOUBLE_DEBIT("Double Debit"),
	FUND_TRANSFER("Fund Transfer"),
	TRANSACTION_DECLINED("Transaction Declined"),
	APP_ISSUE("App Issue"),
	ACCOUNT_BALANCE("Account Balance"),
	GENERAL_INFORMATION("General Information"),
	ATM_CASH_WITHDRAWL("ATM Cash Withdrawal"),
	ATM_PIN("ATM PIN"),
	STATEMENT("Statement"),
	E_COLLECT("eCollect"),
	DISPUTE("Dispute / Chargeback"),
	OTP_NOT_RECEIVED("OTP Not Received"),
	OTHER("Other"),
	JUNK("Junk");

	private final String value;
}
