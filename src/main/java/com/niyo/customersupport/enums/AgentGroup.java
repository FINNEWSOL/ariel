package com.niyo.customersupport.enums;

import lombok.Getter;

@Getter
public enum AgentGroup {
	
	CORPORATE_SUPPORT(2100028509),
	CUSTOMER_SUPPORT(2100027893), 
	PRODUCT_MANAGEMENT(2100015546),
	QA(2100015547),
	SALES(2100015548);
	
	private final Integer value;
	
	private AgentGroup(Integer value) {
		this.value = value;
	}

}
