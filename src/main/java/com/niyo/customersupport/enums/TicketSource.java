package com.niyo.customersupport.enums;

import lombok.Getter;

@Getter
public enum TicketSource {

	EMAIL(1),PORTAL(2),PHONE(3),CHAT(7),MOBIHELP(8),FEEDBACK_WIDGET(9);
	
	private final int value;
	
	private TicketSource(int value) {
		this.value = value;
	}
}
