package com.niyo.customersupport.enums;

public enum TransactionType {
	  C("CR"), //Credit
	  D("DR"); //Debit

	  String naradaFlag;

	  TransactionType(String naradaFlag) {
	    this.naradaFlag = naradaFlag;
	  }

	  public String getNaradaFlag() {
	    return naradaFlag;
	  }
	}