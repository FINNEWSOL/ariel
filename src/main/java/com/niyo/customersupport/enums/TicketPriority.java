package com.niyo.customersupport.enums;

import lombok.Getter;

@Getter
public enum TicketPriority {
	LOW(1),MEDIUM(2),HIGH(3),URGENT(4);
	
	private final int value;
	
	TicketPriority(int value) {
		this.value = value;
	}
}
