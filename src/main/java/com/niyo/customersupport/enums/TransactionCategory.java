package com.niyo.customersupport.enums;

public enum TransactionCategory {
	SPENDS,INCOMES,TRANSFERS,REFUNDS
}
