package com.niyo.customersupport.enums;

import lombok.Getter;

@Getter
public enum PartnerBank {
	
	YES_BANK("YES Bank"),DCB_BANK("DCB Bank");
	
	private final String value;
	
	private PartnerBank(String value) {
		this.value = value;
	}

}
