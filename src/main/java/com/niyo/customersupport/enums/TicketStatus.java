package com.niyo.customersupport.enums;

import lombok.Getter;

@Getter
public enum TicketStatus {
	OPEN(2),PENDING(3),RESOLVED(4),CLOSED(5),WAITING_ON_CUSTOMER(6),WAITING_ON_THIRD_PARTY(7);
	
	private final int value;
	
	TicketStatus(int value) {
		this.value = value;
	}
}
