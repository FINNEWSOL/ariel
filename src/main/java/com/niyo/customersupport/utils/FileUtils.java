package com.niyo.customersupport.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	
	public static boolean deleteFiles(List<File> files) {
		for (File file : files) {
			if (!file.delete()){
				return false;
			}
		}
		return true;
	}
	
	public static List<File> saveMultipartFiles(List<MultipartFile> multipartFiles, String downloadPath) throws IllegalStateException, IOException {
		List<File> files = new ArrayList<>();
		for (MultipartFile multipartFile : multipartFiles) {
			String fileName = multipartFile.getOriginalFilename();
			String destPath = downloadPath + fileName;
			File file = new File(destPath);
			multipartFile.transferTo(file);
			files.add(file);
		}
		return files;
	}

}
