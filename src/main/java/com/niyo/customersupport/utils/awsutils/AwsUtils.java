package com.niyo.customersupport.utils.awsutils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.niyo.customersupport.config.aws.Credentials;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AwsUtils {

	@Autowired
	private AmazonS3 amazonS3;
	
	@Autowired
	TransferManager transferManager;
	
	@Autowired
	private Credentials credentials;
	
	public PutObjectResult uploadfile(String bucketName, String key, File file) {
		PutObjectResult putObjectResult = amazonS3.putObject(bucketName, key, file);
		return putObjectResult;
	}
	
	public void uploadFiles(String bucketName, String key, String folderpath, List<File> files) {
		for (File file: files) {
			PutObjectResult putObjectResult = amazonS3.putObject(bucketName, key, file);
		}
	}
	
	public Download downloadFile(String key, File file) {
		try {
			log.info("downloading: " + key + " into: " + file.getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Download download = transferManager.download(credentials.getBucketname(), key, file);
		return download;
	}
	
}
