package com.niyo.customersupport.utils;

public interface CheckTicketStatus {
	
	default int checkStatus(String status) {
        switch (status) {
            case "open": return 2;
            
            case "Pending": return 3;
            
            case "Resolved": return 4;
            
            case "Closed": return 5;
            
            case "Waiting on Customer": return 6;
            
            case "Waiting on Third Party": return 7;
            
            default: return 2;
        }
    }
	
	default int checkPriority(String priority) {
        switch (priority) {
            case "Low": return 1;
            
            case "Medium": return 2;
            
            case "High": return 3;
            
            case "Urgent": return 4;
            
            default: return 2;
        }
    }

}
