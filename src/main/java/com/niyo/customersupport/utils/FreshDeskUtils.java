package com.niyo.customersupport.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.clients.FreshDeskClient;

@Component
public class FreshDeskUtils {
	
	@Autowired
	private FreshDeskClient freshDeskClient;

	public List<File> downloadAttachmentFromFreshDesk(org.json.JSONArray attachmentArray) throws UnirestException, IOException, JSONException {
		List<File> files = new ArrayList<>();
		int numberOfAttachments = attachmentArray.length();
		for (int i =0; i < numberOfAttachments ; i++) {
			org.json.JSONObject attachment = attachmentArray.getJSONObject(i);
			String url = (String) attachment.get("attachment_url");
			String fileName = System.getProperty("java.io.tmpdir") + File.separator + attachment.getString("name");
			files.add(freshDeskClient.downloadAttachmentFromFreshDesk(url, fileName));
		}
		return files;
	}
}
