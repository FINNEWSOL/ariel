package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class ForbiddenException extends GenericServiceException {

	public ForbiddenException() {
		super("You Are not authorized to do this operation got 403",403);
	}
}
