package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class UnauthorizedAccessException extends GenericServiceException {

	public UnauthorizedAccessException() {
		super("Wrong Access Token or it is expired got 401",401);
	}
}
