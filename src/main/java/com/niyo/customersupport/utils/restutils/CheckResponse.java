package com.niyo.customersupport.utils.restutils;

import com.niyo.customersupport.utils.restutils.exceptions.ForbiddenException;
import com.niyo.customersupport.utils.restutils.exceptions.GenericServiceException;
import com.niyo.customersupport.utils.restutils.exceptions.InvalidInputException;
import com.niyo.customersupport.utils.restutils.exceptions.InvalidResourceException;
import com.niyo.customersupport.utils.restutils.exceptions.UnauthorizedAccessException;

public interface CheckResponse {
	
	default void checkResponse(Integer status) throws GenericServiceException {
        switch (status) {
            case 200:
            case 201:
            case 204:
                //request successful, do nothing
                break;
            case 400:
                throw new InvalidInputException();
            case 401:
                throw new UnauthorizedAccessException();
            case 403:
            	throw new ForbiddenException();
            case 404:
                throw new InvalidResourceException();
            //case 422:
            //    throw new InvalidInputException(status);
            default:
                throw new GenericServiceException(status);
        }
    }

}
