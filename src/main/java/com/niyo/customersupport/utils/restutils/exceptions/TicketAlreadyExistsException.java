package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class TicketAlreadyExistsException extends Exception {

	String message;
	
	public TicketAlreadyExistsException(){
		this.message = "Ticket is already exists in niyo support system no need to create duplicate";
	}
}
