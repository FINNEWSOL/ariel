package com.niyo.customersupport.utils.restutils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.management.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.niyo.customersupport.services.StaticResourceService;
import com.niyo.customersupport.utils.restutils.exceptions.UnauthorizedAccessException;

import lombok.extern.slf4j.Slf4j;

@Component
@EnableRetry(proxyTargetClass=true)
@Slf4j
public class RestUtil implements CheckResponse {
	
	@Autowired
	StaticResourceService staticResourceService;

	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode post(String url, String body, String username, String password) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.post(url).header("content-type", MediaType.APPLICATION_JSON_VALUE).basicAuth(username, password).body(body).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode get(String url, String username, String password) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.get(url).basicAuth(username, password).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode post(String url, String body) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.post(url).header("content-type", MediaType.APPLICATION_JSON_VALUE).body(body).asJson();
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> Headers postGetHeader(String url, String body) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.post(url).header("content-type", MediaType.APPLICATION_JSON_VALUE).body(body).asJson();
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getHeaders();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode get(String url) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.get(url).asJson();
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode get(String url, Map<String,String> headers) throws UnirestException {
		log.info("GET Request url : " + url);
		HttpResponse<JsonNode> response = Unirest.get(url).headers(headers).asJson();
		Headers responseHeaders = response.getHeaders();
		log.info("Headers: " + responseHeaders);
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode post(String url, String body, Map<String,String> headers) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.post(url).headers(headers).body(body).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode multipartPost(String url, Map<String,Object> body, String username, String password, List<File> files) throws UnirestException {
		log.info("Request: " + body.toString());
		HttpResponse<JsonNode> response = null;
		if (files == null) {
			response = Unirest.post(url).basicAuth(username, password).header("content-type", MediaType.MULTIPART_FORM_DATA_VALUE).fields(body).asJson();
		} else {
			for (File file: files) {
				try {
					log.info("Files to freshdesk: " + file.getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			response = Unirest.post(url).basicAuth(username, password).fields(body).field("attachments[]", files).asJson();
		}
		
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		checkResponse(response.getStatus());
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode multipartPost2(String url, Map<String,Object> body, String username, String password, List<File> files) throws UnirestException {
		//HttpResponse<JsonNode> response = Unirest.post(url).basicAuth(username, password).fields(body).field("attachments[]", new File("/home/admin1/attachment2.txt")).field("attachments[]", new File("/home/admin1/attachment.txt")).asJson();
		HttpResponse<JsonNode> response = Unirest.post(url).basicAuth(username, password).fields(body).field("attachments[]", files).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		checkResponse(response.getStatus());
		return response.getBody();
	}
	
//	public <T> JsonNode multipartPost(String url, Map<String,Object> body, String username, String password, List<File> files) throws UnirestException {
//		HttpResponse<JsonNode> response = Unirest.post(url).basicAuth(username, password).fields(body).field("attachments[]", files).asJson();
//		return response.getBody();
//	}
	
//	public File download(String url, String fileName) throws UnirestException, IOException {
//		HttpResponse<InputStream> response = Unirest.get(url).asBinary();
//		File file = new File(GeneralConfig.getDownloadAttachmentPath() + fileName);
//		InputStream responseData = response.getBody();
//		OutputStream outputStream = new FileOutputStream(file);
//		int read = 0;
//		byte[] bytes = new byte[1024];
//		while ((read = responseData.read(bytes)) != -1) {
//			outputStream.write(bytes, 0, read);
//		}
//		outputStream.close();
//		return file;
//	}
	
	public <T> JsonNode put(String url, Map<String,Object> body, String username, String password) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.put(url).basicAuth(username, password).header("content-type", MediaType.MULTIPART_FORM_DATA_VALUE).fields(body).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		checkResponse(response.getStatus());
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode patch(String url, String body, Map<String,String> headers) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.patch(url).headers(headers).body(body).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> HttpResponse<JsonNode> getWithQueryParams(String url, Map<String, Object> parameters) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.get(url).queryString(parameters).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response;
	}
	
	@Retryable(maxAttempts = 2, backoff = @Backoff(delay = 2000))
	public <T> JsonNode post(String url, Object body, String username, String password) throws UnirestException {
		HttpResponse<JsonNode> response = Unirest.post(url).basicAuth(username, password).body(body).asJson();
		log.info(response.getBody().toString());
		log.info(response.getStatusText());
		try{
			checkResponse(response.getStatus());
		} catch (UnauthorizedAccessException e) {
			staticResourceService.refreshToken();
		}
		return response.getBody();
	}
}
