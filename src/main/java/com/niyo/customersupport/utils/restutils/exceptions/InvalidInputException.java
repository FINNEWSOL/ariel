package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class InvalidInputException extends GenericServiceException {

	public InvalidInputException() {
		super("Invalid request body: Got 400 response",400);
	}
}
