package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class InvalidResourceException extends GenericServiceException {

	public InvalidResourceException() {
		super("Wrong url got 404",404);
	}
}
