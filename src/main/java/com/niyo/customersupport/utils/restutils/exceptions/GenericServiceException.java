package com.niyo.customersupport.utils.restutils.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GenericServiceException extends RuntimeException {

	private String message;
	
	private int status;
	
	public GenericServiceException(int status) {
		this.status = status;
		this.message = "Got Response " + String.valueOf(status);
	}
	
}
