package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class ReplyAlreadyExistsException extends Exception{

	String message;
	
	public ReplyAlreadyExistsException() {
		this.message = "This reply is already exists in niyo support system";
	}
}
