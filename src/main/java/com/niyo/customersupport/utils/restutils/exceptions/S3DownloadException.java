package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class S3DownloadException extends Exception {

	String message;
	
	public S3DownloadException() {
		this.message = "Download from s3 exception";
	}
}