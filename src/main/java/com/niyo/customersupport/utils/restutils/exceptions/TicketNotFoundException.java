package com.niyo.customersupport.utils.restutils.exceptions;

@SuppressWarnings("serial")
public class TicketNotFoundException extends Exception{

	String message;
	
	public TicketNotFoundException(){
		this.message = "Ticket doesn't exists in niyo support system";
	}
}
