package com.niyo.customersupport.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper=false)
public class CorporateCreationFailedException extends Exception {

	private String msg;
	
	public CorporateCreationFailedException(String msg) {
		this.msg = msg;
	}
}
