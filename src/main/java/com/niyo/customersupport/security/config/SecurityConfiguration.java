package com.niyo.customersupport.security.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;

import com.niyo.customersupport.security.Http401UnauthorizedEntryPoint;
import com.niyo.customersupport.security.SHATokenProvider;
import com.niyo.customersupport.security.XAuthTokenConfigurer;
import com.niyo.customersupport.security.services.NiyoUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private Http401UnauthorizedEntryPoint authenticationEntryPoint;
	
	@Autowired
	private NiyoUserDetailsService userDetailsService;
	
	@Autowired
	private SHATokenProvider shaTokenProvider;
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }
	
//	@Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring()
//            .antMatchers("/api/v1/tickets/{crn}")
//            .antMatchers("/api/v1/tickets/event/mail");
//    }
	
	 @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	            .exceptionHandling()
	            .authenticationEntryPoint(authenticationEntryPoint)
	        .and()
	            .csrf()
	            .disable()
	            .headers()
	            .frameOptions()
	            .disable()
	        .and()
	            .sessionManagement()
	            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	        .and()
	            .authorizeRequests()
	            .antMatchers("/health/**").permitAll()
	            .antMatchers("/api/app/v1/tickets/**").authenticated()
	        .and()
	            .apply(securityConfigurerAdapter());
	    }
	 
	 private XAuthTokenConfigurer securityConfigurerAdapter() {
	      return new XAuthTokenConfigurer(userDetailsService,shaTokenProvider);
	    }
	 
	 @Bean
	 public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
	      return new SecurityEvaluationContextExtension();
	 }
}
