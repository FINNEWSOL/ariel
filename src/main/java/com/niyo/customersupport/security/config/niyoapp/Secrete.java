package com.niyo.customersupport.security.config.niyoapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "spring.secreteconf")
public class Secrete {

	private String customersecrete;
	
	private String arielsecrete;
}
