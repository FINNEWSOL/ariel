package com.niyo.customersupport.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.niyo.customersupport.security.config.niyoapp.Secrete;

@Configuration
public class XAuthConfiguration {
	
	@Autowired
	private Secrete secrete;

	@Bean
    public SHATokenProvider shaTokenProvider(){
        String customerSecret = secrete.getCustomersecrete();
        String arielApiSecret = secrete.getArielsecrete();
        return new SHATokenProvider(customerSecret,arielApiSecret);
    }
}
