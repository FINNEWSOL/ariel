package com.niyo.customersupport.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrentUser extends org.springframework.security.core.userdetails.User {

  private String id;

  private String firstName;

  private String lastName;

  private String login;

  private String email;

  private Set<String> branches = new HashSet<>();

  private Set<String> roles = new HashSet<>();

  private String corporateCode;

  public CurrentUser(String username, Collection<? extends GrantedAuthority> authorities,
                     String id, String firstName, String lastName, String login, String email, Set<String> branches, Set<String> roles) {
    super(username, "", authorities);
    setFirstName(firstName);
    setLastName(lastName);
    setId(id);
    setEmail(email);
    setLogin(login);
    setBranches(branches);
    setRoles(roles);
  }

  public CurrentUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
    super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
  }

  public CurrentUser(String username, Collection<? extends GrantedAuthority> authorities,
                     String id, String firstName, String lastName, String login, String email, Set<String> branches, Set<String> roles, String corporateCode) {
    this(username, authorities, id, firstName, lastName, login, email, branches, roles);
    setCorporateCode(corporateCode);
  }
}
