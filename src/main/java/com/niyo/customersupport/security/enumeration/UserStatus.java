package com.niyo.customersupport.security.enumeration;

/**
 * The UserStatus enumeration.
 */
public enum UserStatus {
    DRAFT, SUBMIT, ACTIVE, REJECT, BLOCK
}
