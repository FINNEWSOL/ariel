package com.niyo.customersupport.security;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.niyo.customersupport.security.pojos.mongo.AppUser;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;

public final class SHATokenProvider {

	private static final String HMAC_ALGO = "HmacSHA256";
    private static final String SEPARATOR = ".";
    private static final String SEPARATOR_SPLITTER = "\\.";
    private final Mac customerHmac;
    private final Mac arielApiHmac;
    
	public SHATokenProvider(String customerSecret, String arielApiSecret) {
		try {
            customerHmac = Mac.getInstance(HMAC_ALGO);
            customerHmac.init(new SecretKeySpec(customerSecret.getBytes(), HMAC_ALGO));
            arielApiHmac = Mac.getInstance(HMAC_ALGO);
            arielApiHmac.init(new SecretKeySpec(arielApiSecret.getBytes(), HMAC_ALGO));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalStateException("failed to initialize HMAC: " + e.getMessage(), e);
        }
	}
	
	private byte[] fromBase64(String content) {
        return DatatypeConverter.parseBase64Binary(content);
    }

    private String toBase64(byte[] content) {
        return DatatypeConverter.printBase64Binary(content);
    }
    
    public <T> T fromJson(byte[] userBytes, Class<T> clazz) {
        try {
            return (T) new ObjectMapper().readValue(userBytes, clazz);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    
    private byte[] createHmac(byte[] content, AppUser.USER_TYPE user_type) {
        byte[] signature = null;
        if (user_type.equals(AppUser.USER_TYPE.C)) {
          synchronized (customerHmac) {
            signature = customerHmac.doFinal(content);
          }
        } else if (user_type.equals(AppUser.USER_TYPE.CS)) {
      	  synchronized (arielApiHmac) {
                signature = arielApiHmac.doFinal(content);
              } 
        }
        if (signature != null) {
          return signature;
        }
        throw new UnsupportedOperationException("token_type +: " + user_type + " not implemented!");
      }
    
	public <T> T parseObjectFromToken(String token, Class<T> clazz) {
        final String[] parts = token.split(SEPARATOR_SPLITTER);
        if (parts.length == 3 && parts[0].length() > 0 && parts[1].length() > 0 && parts[2].length() > 0) {
            try {
                final byte[] tokenTypeBytes = fromBase64(parts[0]);
                final byte[] userBytes = fromBase64(parts[1]);
                final byte[] hash = fromBase64(parts[2]);
                boolean validHash = Arrays.equals(createHmac(userBytes, AppUser.USER_TYPE.valueOf(new String(tokenTypeBytes))), hash);
                if (validHash) {
                    T object = fromJson(userBytes, clazz);
                    return object;
                }
            } catch (IllegalArgumentException e) {
              e.printStackTrace();
            }
        }
        return null;
    }
	
	/**
     * 1) Validate if token is expired or not
     * 2) Check if user has roles
     * 3) Check if user role match with the one in db
     *
     * @param shaToken
     * @param userDetails
     * @param tokenValidity
	 * @throws NiyopayAuthenticationException 
     */
    public void validateToken(SHAToken shaToken, UserDetails userDetails, long tokenValidity) throws NiyopayAuthenticationException {
        long ageOfToken = System.currentTimeMillis() - shaToken.getGeneratedTime();
        if(ageOfToken >= tokenValidity){
            throw new NiyopayAuthenticationException("Token expired --> require login");
        }
        
        if(CollectionUtils.isEmpty(userDetails.getAuthorities())){
            //TODO need discussion
            throw new NiyopayAuthenticationException("No Roles for user.");
        }
    }
}
