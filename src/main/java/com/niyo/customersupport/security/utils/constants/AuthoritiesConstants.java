package com.niyo.customersupport.security.utils.constants;

public final class AuthoritiesConstants {
	
	public static final String ADMIN = "ROLE_ADMIN";
	  public static final String USER = "ROLE_USER";
	  public static final String ANONYMOUS = "ROLE_ANONYMOUS";
	  public static final String CUSTOMER = "ROLE_CUSTOMER";

	  public static final String OPS_EXECUTIVE = "ROLE_OPS_EXECUTIVE";
	  public static final String OPS_MANAGER = "ROLE_OPS_MANAGER";
	  public static final String SALES_EXECUTIVE = "ROLE_SALES_EXECUTIVE";
	  public static final String SALES_MANAGER = "ROLE_SALES_MANAGER";
	  public static final String SALES_HEAD = "ROLE_SALES_HEAD";
	  public static final String CUSTOMER_SUPPORT_EXECUTIVE = "ROLE_CUSTOMER_SUPPORT_EXECUTIVE";
	  public static final String CUSTOMER_SUPPORT_MANAGER = "ROLE_CUSTOMER_SUPPORT_MANAGER";

	  public static final String VIEW_PERMISSION = "ROLE_VIEW_PERMISSION";

	  public static final String VIEW_ROLE = "ROLE_VIEW_ROLE";
	  public static final String CREATE_ROLE = "ROLE_CREATE_ROLE";
	  public static final String UPDATE_ROLE = "ROLE_UPDATE_ROLE";
	  public static final String DELETE_ROLE = "ROLE_DELETE_ROLE";

	  public static final String VIEW_APPUSER = "ROLE_VIEW_APPUSER";
	  public static final String CREATE_APPUSER = "ROLE_CREATE_APPUSER";
	  public static final String UPDATE_APPUSER = "ROLE_UPDATE_APPUSER";
	  public static final String APPROVE_APPUSER = "ROLE_APPROVE_APPUSER";
	  public static final String DELETE_APPUSER = "ROLE_DELETE_APPUSER";

	  public static final String VIEW_BRANCH = "ROLE_VIEW_BRANCH";
	  public static final String CREATE_BRANCH = "ROLE_CREATE_BRANCH";
	  public static final String UPDATE_BRANCH = "ROLE_UPDATE_BRANCH";
	  public static final String APPROVE_BRANCH = "ROLE_APPROVE_BRANCH";

	  public static final String UPLOAD_CARDKITS = "ROLE_UPLOAD_CARDKITS";
	  public static final String STOCK_CARDKITS = "ROLE_STOCK_CARDKITS";
	  public static final String ASSIGN_CARDKITS_TO_BRANCH = "ROLE_ASSIGN_CARDKITS_TO_BRANCH";
	  public static final String VIEW_CARDKITS = "ROLE_VIEW_CARDKITS";
	  public static final String ASSIGN_CARDKITS = "ROLE_ASSIGN_CARDKITS";

	  public static final String VIEW_CORPORATE = "ROLE_VIEW_CORPORATE";
	  public static final String CREATE_CORPORATE = "ROLE_CREATE_CORPORATE";
	  public static final String UPDATE_CORPORATE = "ROLE_UPDATE_CORPORATE";
	  public static final String APPROVE_CORPORATE = "ROLE_APPROVE_CORPORATE";

	  public static final String VIEW_CUSTOMER = "ROLE_VIEW_CUSTOMER";
	  public static final String VIEW_CUSTOMER_PAYEES_TRANSFER_REQUESTS = "ROLE_VIEW_CUSTOMER_PAYEES_TRANSFER_REQUESTS";
	  public static final String VIEW_CUSTOMER_TRANSACTIONS = "ROLE_VIEW_CUSTOMER_TRANSACTIONS";
	  public static final String UPDATE_CUSTOMER_TRANSACTIONS = "ROLE_UPDATE_CUSTOMER_TRANSACTIONS";
	  public static final String CREATE_CUSTOMER = "ROLE_CREATE_CUSTOMER";
	  public static final String UPDATE_CUSTOMER = "ROLE_UPDATE_CUSTOMER";
	  public static final String APPROVE_CUSTOMER = "ROLE_APPROVE_CUSTOMER";

	  public static final String VIEW_KYC = "ROLE_VIEW_KYC";
	  public static final String APPROVE_KYC = "ROLE_APPROVE_KYC";

	  public static final String API_ACCESS = "ROLE_API_USER";
	  public static final String GREYTIP_API_ACCESS = "ROLE_GREYTIP_API_USER";


	  public static final String VIEW_AUDIT_LOGS = "ROLE_VIEW_AUDIT_LOGS";
	  public static final String VIEW_CUSTOMER_LOGS = "ROLE_VIEW_CUSTOMER_LOGS";
	  public static final String VIEW_CUSTOMER_HISTORY = "ROLE_VIEW_CUSTOMER_HISTORY";

	  public static final String VIEW_ACCOUNT_NUMBERS = "ROLE_VIEW_ACCOUNT_NUMBERS";
	  public static final String GENERATE_CRN = "ROLE_GENERATE_CRN";

	  public static final String VIEW_SALARY_UPLOAD = "ROLE_VIEW_SALARY_UPLOAD";
	  public static final String APPROVE_SALARY_UPLOAD = "ROLE_APPROVE_SALARY_UPLOAD";
	  public static final String UPLOAD_SALARY_SHEET = "ROLE_UPLOAD_SALARY_SHEET";



	  public static final String CREATE_PRODUCT = "ROLE_CREATE_PRODUCT";
	  public static final String UPDATE_PRODUCT = "ROLE_UPDATE_PRODUCT";
	  public static final String VIEW_PRODUCT = "ROLE_VIEW_PRODUCT";
	  public static final String REMOVE_PRODUCT = "ROLE_REMOVE_PRODUCT";

	  public static final String VIEW_PROGRAM = "ROLE_VIEW_PROGRAM";
	  public static final String CREATE_PROGRAM = "ROLE_CREATE_PROGRAM";
	  public static final String UPDATE_PROGRAM = "ROLE_UPDATE_PROGRAM";
	  public static final String REMOVE_PROGRAM = "ROLE_REMOVE_PROGRAM";
	  public static final String REMOVE_CORPORATE_CONTACT = "ROLE_REMOVE_CORPORATE_CONTACT";

	  public static final String UPLOAD_IFSC = "ROLE_UPLOAD_IFSC";
	  public static final String BULKSEND_SMS = "ROLE_BULKSEND_SMS";
	  public static final String BULKSEND_EMAIL = "ROLE_BULKSEND_EMAIL";
	  public static final String EXECUTE_TEST_CONTROLLER = "ROLE_EXECUTE_TEST_CONTROLLER";
	  public static final String NOTIFY_CUSTOMER = "ROLE_NOTIFY_CUSTOMER";
	  public static final String WALLET_PROMO = "ROLE_WALLET_PROMO";
	  public static final String WALLET_RETENTION = "ROLE_WALLET_RETENTION";
	  public static final String ADD_FUNDS_CORP_WALLET = "ROLE_ADD_FUNDS_CORP_WALLET";
	  public static final String VIEW_N15 = "ROLE_VIEW_N15";
	  public static final String ADD_REVOKE_CARD_FUNDS = "ROLE_ADD_REVOKE_CARD_FUNDS";



	  public static final String CP_VIEW_USER = "ROLE_CP_VIEW_USER";
	  public static final String CP_CREATE_USER = "ROLE_CP_CREATE_USER";
	  public static final String CP_UPDATE_USER = "ROLE_CP_UPDATE_USER";
	  public static final String CP_DELETE_USER = "ROLE_CP_DELETE_USER";
	  public static final String CP_VIEW_EMPLOYEE = "ROLE_CP_VIEW_EMPLOYEE";
	  public static final String CP_UPDATE_EMPLOYEE = "ROLE_CP_UPDATE_EMPLOYEE";
	  public static final String CP_VIEW_TRANSACTIONS = "ROLE_CP_VIEW_TRANSACTIONS";
	  public static final String CP_VIEW_PROGRAM = "ROLE_CP_VIEW_PROGRAM";
	  public static final String CP_UPDATE_PROGRAM = "ROLE_CP_UPDATE_PROGRAM";
	  public static final String CP_VIEW_CORPORATE = "ROLE_CP_VIEW_CORPORATE";
	  public static final String CP_VIEW_PERMISSION = "ROLE_CP_VIEW_PERMISSION";
	  public static final String CP_ADD_REVOKE_CARD_FUNDS = "ROLE_CP_ADD_REVOKE_CARD_FUNDS";
	  public static final String BATCH_ONBOARDING = "ROLE_BATCH_ONBOARDING";
	  public static final String CARDOPS_TXN_UPLOAD = "ROLE_CARDOPS_TXN_UPLOAD";
	  public static final String CARDOPS_ACCOUNT_BALANCE_UPLOAD = "ROLE_CARDOPS_ACCOUNT_BALANCE_UPLOAD";
	  public static final String CREDIT_N15 = "ROLE_CREDIT_N15";
	  public static final String SETTLE_N15 = "ROLE_SETTLE_N15";

	  public static final String CP_VIEW_CORPORATE_TRANSACTIONS = "ROLE_CP_VIEW_CORPORATE_TRANSACTIONS";
	  public static final String CP_UPLOAD_CARD_FUNDING = "ROLE_CP_UPLOAD_CARD_FUNDING";
	  public static final String CP_VIEW_CARD_FUNDING = "ROLE_CP_VIEW_CARD_FUNDING";
	  public static final String CP_APPROVE_CARD_FUNDING = "ROLE_CP_APPROVE_CARD_FUNDING";

	  public static final String VIEW_ECOLLECT_ENTRIES = "ROLE_VIEW_ECOLLECT_ENTRIES";
	  public static final String UPLOAD_ECOLLECT_FILE = "ROLE_UPLOAD_ECOLLECT_FILE";
	  public static final String APPROVE_ECOLLECT_ENTRIES = "ROLE_APPROVE_ECOLLECT_ENTRIES";

	  public static final String CP_CREATE_EMPLOYEE = "ROLE_CP_CREATE_EMPLOYEE";

	  public static final String VIEW_NOTIFICATION_TEMPLATE = "ROLE_VIEW_NOTIFICATION_TEMPLATE";
	  public static final String CREATE_NOTIFICATION_TEMPLATE = "ROLE_CREATE_NOTIFICATION_TEMPLATE";
	  public static final String UPDATE_NOTIFICATION_TEMPLATE = "ROLE_UPDATE_NOTIFICATION_TEMPLATE";

	  public static final String CS_ARIEL_API_ACCESS = "ROLE_CS_ARIEL_API_ACCESS";

	  private AuthoritiesConstants() {}

}
