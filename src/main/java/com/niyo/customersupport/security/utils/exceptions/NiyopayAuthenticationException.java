package com.niyo.customersupport.security.utils.exceptions;

import javax.naming.AuthenticationException;

public class NiyopayAuthenticationException extends AuthenticationException {
	
	public NiyopayAuthenticationException(String message){
		super (message);
	}

}
