package com.niyo.customersupport.security;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.niyo.customersupport.security.services.NiyoUserDetailsService;

public class XAuthTokenConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity>{
	
	private NiyoUserDetailsService niyoUserDetailsService;
	
	private SHATokenProvider shaTokenProvider;

	public XAuthTokenConfigurer(NiyoUserDetailsService userDetailsService, SHATokenProvider shaTokenProvider) {
		this.niyoUserDetailsService = userDetailsService;
		this.shaTokenProvider = shaTokenProvider;
	}

	@Override
	  public void configure(HttpSecurity http) throws Exception {
	    XAuthTokenFilter customFilter = new XAuthTokenFilter(niyoUserDetailsService, shaTokenProvider);
	    http.addFilterBefore( customFilter, UsernamePasswordAuthenticationFilter.class);
	  }
}
