package com.niyo.customersupport.security.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.niyo.customersupport.security.repositories.mongo.PartnerBankMappingRepository;

@Service
public class PartnerBankMappingService {
	
	@Autowired
	private PartnerBankMappingRepository partnerBankMappingRepository;

	@Cacheable(value = "apiCache#86400", key = "'all-partnerBankMapping-prefixes'", unless = "#result == null")
	public List<String> getAllBankPrefixes() {
	    List<String> prefixes = new ArrayList<>();
	    partnerBankMappingRepository.findAll().forEach(partnerBankMapping -> {
	      prefixes.add(partnerBankMapping.getPrefix());
	    });
	    return prefixes;
	}
}
