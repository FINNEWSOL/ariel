package com.niyo.customersupport.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.niyo.customersupport.security.pojos.mongo.AppUser;
import com.niyo.customersupport.security.pojos.mongo.Config;
import com.niyo.customersupport.security.repositories.mongo.ConfigRepository;

@Service
public class ConfigService {
	
	static final String TOKEN_LIMITS_KEY = "TOKEN_EX";
	
	@Autowired
	private ConfigRepository configRepository;

	@Cacheable(value = "apiCache#86400", key = "'TokenExpiry-'.concat(#type)", unless = "#result == 0")
	public long getTokenExpiry(AppUser.USER_TYPE type) {
		Config config = configRepository.findOne(TOKEN_LIMITS_KEY);
		String val = config.getConfigs().get(type.toString());
		if (!StringUtils.isEmpty(val)) {
			return Long.parseLong(val);
		}
		return 0;
	}
}
