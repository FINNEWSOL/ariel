package com.niyo.customersupport.security.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.niyo.customersupport.security.CurrentUser;
import com.niyo.customersupport.security.enumeration.UserStatus;
import com.niyo.customersupport.security.pojos.mongo.ApiUser;
import com.niyo.customersupport.security.pojos.mongo.AppUser;
import com.niyo.customersupport.security.pojos.mongo.Authority;
import com.niyo.customersupport.security.pojos.mongo.Customer;
import com.niyo.customersupport.security.repositories.mongo.ApiUserRepository;
import com.niyo.customersupport.security.repositories.mongo.CustomerRepository;
import com.niyo.customersupport.security.utils.constants.AuthoritiesConstants;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;
import com.niyo.customersupport.security.utils.exceptions.UserNotActivatedException;

@Component("userDetailsService")
public class NiyoUserDetailsService implements UserDetailsService {
	
	@Autowired
	private ConfigService configService;
	
	@Autowired
	private PartnerBankMappingService partnerBankMappingService;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private ApiUserRepository apiUserRepository;

	@Override
	@Deprecated
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		throw new UnsupportedOperationException();
	}

	public long getTokenExpiry(AppUser.USER_TYPE userType) {
		return configService.getTokenExpiry(userType);
	}

	public UserDetails loadCustomerUserDetails(String login, String url) throws NiyopayAuthenticationException {
		List<String> list = partnerBankMappingService.getAllBankPrefixes();
		boolean hasPrefixFoundInUrl = false;
		if ((url != null)) {
			for (String prefix : list) {
				if (url.contains(prefix)) {
					hasPrefixFoundInUrl = true;
					break;
				}
			}
		}
		if (hasPrefixFoundInUrl) {
		      if (!url.contains(login)) {
		        throw new NiyopayAuthenticationException("Authentication Failure");
		      }
		    }
		    Optional<Customer> userFromDatabase = customerRepository.findOneByCrn(login);
		    return getCustomerUserDetails(userFromDatabase);
//		Customer customer = customerRepository.findByCrnAndStatus(login, "ACTIVE");
//		return customer;
	}
	
	
	private UserDetails getCustomerUserDetails(Optional<Customer> userFromDatabase) {
	    return userFromDatabase.map(user -> {
	      if (UserStatus.ACTIVE != user.getStatus()) {
	        throw new UserNotActivatedException("User with CRN : " + user.getCrn() + " was not activated");
	      }
	      List<GrantedAuthority> grantedAuthorities =
	        user.getAuthorities().stream().map(authority -> new SimpleGrantedAuthority(authority.getName())).collect(Collectors.toList());
	      Set<Authority> authorities = user.getAuthorities();
	      Set<String> authoritiesStrings = new HashSet<String>();
	      for (Authority auth : authorities) {
	        authoritiesStrings.add(auth.getName());
	      }
	      return new CurrentUser(user.getCrn(), grantedAuthorities, null, null, null, user.getLogin(), null, null, authoritiesStrings);
	    }).orElseThrow(() -> new UsernameNotFoundException("User not found in the database"));
	  }
	
	public UserDetails loadArielUserDetails(String apiKey, String secret) throws NiyopayAuthenticationException {
	    Optional<ApiUser> apiUserOptional;
	    if (secret == null) {
	      apiUserOptional = apiUserRepository.getApiUser(apiKey);
	    } else {
	      apiUserOptional = apiUserRepository.getApiUser(apiKey, secret);
	    }
	    return apiUserOptional.map(apiUser -> {
	      List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
	      grantedAuthorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.CS_ARIEL_API_ACCESS));
	      return new CurrentUser(apiUser.getKey(), grantedAuthorities, null, null, null, apiUser.getKey(), null, null, null);
	    }).orElseThrow(() -> new NiyopayAuthenticationException("Unknown Client"));

	  }
}
