package com.niyo.customersupport.security;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.niyo.customersupport.security.pojos.mongo.AppUser;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class SHAToken implements Serializable {

  /**
   * Login can be crn/email/apikey
   */
  @NotNull
  @Getter
  private String login;

  @NotNull
  @Getter
  private long generatedTime;

  @NotNull
  @Getter
  private AppUser.USER_TYPE user_type;

  @NotNull
  @Getter
  private String corporateCode;

  public static SHAToken getSHATokenObject(UserDetails userDetails, AppUser.USER_TYPE USER_type) {
    return new SHAToken(userDetails.getUsername(), System.currentTimeMillis(), USER_type, null);
  }

  public static SHAToken getSHATokenObject(CurrentUser currentUser, AppUser.USER_TYPE USER_type) {
    return new SHAToken(currentUser.getUsername(), System.currentTimeMillis(), USER_type, currentUser.getCorporateCode());
  }
}
