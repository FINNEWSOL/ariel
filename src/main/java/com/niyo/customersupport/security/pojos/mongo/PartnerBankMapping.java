package com.niyo.customersupport.security.pojos.mongo;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;
import lombok.ToString;

@Document(collection = "partner_bank_mappings")
@Data
@ToString
public class PartnerBankMapping implements Serializable {

  private static final long serialVersionUID = 1L;

//  @Id
//  private PartnerBank partnerBank;

  @Field("prefix")
  private String prefix;

//  @Field("ifsc")
//  private String ifsc;
//
//  @Field("bankname")
//  private String bankName;
//
//  @Field("is_corp_mapping")
//  private Boolean isCorporateMapping;
//
//  @Field("ekyc_service_provider")
//  private EkycServiceProvider ekycServiceProvider;
//
//  @Field("limit_configs")
//  @JsonIgnore
//  private HashMap<ParterBankConfigKey, ParterBankConfigDefaults> limitConfigs;
}
