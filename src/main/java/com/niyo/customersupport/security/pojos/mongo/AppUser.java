package com.niyo.customersupport.security.pojos.mongo;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import com.niyo.customersupport.security.enumeration.UserStatus;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AppUser {

  private static final long serialVersionUID = 1L;

  @Id
  private String id;

  @Size(min = 5, max = 256)
  private String login;

  private USER_TYPE type;

  @Field("status")
  private UserStatus status;
  
  private Set<Authority> authorities = new HashSet<>();
  
  @Field("title")
  private String title;

  @Field("first_name")
  private String firstName;

  @Field("middle_name")
  private String middleName;

  @Field("last_name")
  private String lastName;

  @Field("nick_name")
  private String nickName;

//  @Field(value = "last_login_time")
//  private ZonedDateTime lastLoginTime;

  @Field("is_test_user")
  private Boolean isTestUser;

  public enum USER_TYPE {
    A, // Used for agent
    C, // User For Customer
    S, // For api calls. M-M
    CP, // For Corporate portal users
    GT, // For Greytip api
    CS; // For customer support api

    public static USER_TYPE get(String type) throws NiyopayAuthenticationException {
      if (type.equals(A.toString())) {
        return A;
      } else if (type.equals(C.toString())) {
        return C;
      }
      throw new NiyopayAuthenticationException("Cannot determine token type in request");
    }
  }
}
