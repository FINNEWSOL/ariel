package com.niyo.customersupport.security.pojos.mongo;

import java.time.ZonedDateTime;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Document(collection = "customers")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@ToString
public class Customer extends AppUser {
	
	@Field("crn")
	private String crn;

//	@Field("enrolled_at")
//	private ZonedDateTime enrolledAt;
//
//	@Field("approved_at")
//	private ZonedDateTime approvedAt;
}
