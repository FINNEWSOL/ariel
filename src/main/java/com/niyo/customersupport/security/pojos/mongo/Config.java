package com.niyo.customersupport.security.pojos.mongo;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Document(collection = "config")
public class Config implements Serializable {

    @Id
    private String name;

    Map<String, String> configs;
}