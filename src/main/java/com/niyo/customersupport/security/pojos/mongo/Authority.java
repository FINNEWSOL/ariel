package com.niyo.customersupport.security.pojos.mongo;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.niyo.customersupport.security.pojos.mongo.AppUser.USER_TYPE;

import lombok.Data;

/**
 * An authority (a security role) used by Spring Security.
 */
@Data
@Document(collection = "jhi_authority")
@JsonInclude(Include.NON_NULL)
public class Authority implements Serializable {

    static final long serialVersionUID = 1L;

    @NotNull
    @Id
    private String name;
    
    private AppUser.USER_TYPE userType;

    private String description;
    
    private Set<String> permissions;

}