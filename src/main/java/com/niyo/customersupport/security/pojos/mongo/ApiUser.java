package com.niyo.customersupport.security.pojos.mongo;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * A ApiUser.
 */

@Document(collection = "api_users")
@Data
public class ApiUser implements Serializable {

  @Id
  private String key;

  private String secret;

  private String description;

}