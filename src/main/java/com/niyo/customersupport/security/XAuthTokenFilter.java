package com.niyo.customersupport.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.niyo.customersupport.security.services.NiyoUserDetailsService;
import com.niyo.customersupport.security.utils.constants.Constants;
import com.niyo.customersupport.security.utils.exceptions.NiyopayAuthenticationException;
import com.niyo.customersupport.security.utils.exceptions.UserNotActivatedException;

public class XAuthTokenFilter extends GenericFilterBean {
	
	private static final String NULL_STRING = "null";
	
	private NiyoUserDetailsService niyoUserDetailsService;
	
	private SHATokenProvider shaTokenProvider;
	
	public XAuthTokenFilter(NiyoUserDetailsService niyoUserDetailsService, SHATokenProvider shaTokenProvider) {
		this.niyoUserDetailsService = niyoUserDetailsService;
		this.shaTokenProvider = shaTokenProvider;
	}

	
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
	    try {
	      HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
	      if ("OPTIONS".equals(httpServletRequest.getMethod()) ) {
	        return;
	      }
	      String url = httpServletRequest.getRequestURI();
	      String authToken = httpServletRequest.getHeader(Constants.XAUTH_TOKEN_HEADER_NAME);
	      if (StringUtils.hasText(authToken) && !authToken.equals(NULL_STRING)) {
	        SHAToken shaToken = shaTokenProvider.parseObjectFromToken(authToken, SHAToken.class);
	        if (shaToken != null) {
	        	org.springframework.security.core.userdetails.UserDetails userDetails = null;
	          long tokenExpiry = niyoUserDetailsService.getTokenExpiry(shaToken.getUser_type());
	          switch (shaToken.getUser_type()) {
	            case C:
	              /**
	               * Token handling for Customer
	               */
	              try {
	            	  userDetails = niyoUserDetailsService.loadCustomerUserDetails(shaToken.getLogin(), url);
	              } catch (UsernameNotFoundException e) {
	                HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
	                httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
	                return;
	              }
	              break;
	          }
	          if (userDetails != null) {
	            shaTokenProvider.validateToken(shaToken, userDetails, tokenExpiry);
	            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
	            SecurityContextHolder.getContext().setAuthentication(token);
	          }
	        } else {
	          //TODO Unknown reason
	        
	          //logger.error("Cannot parse token -  " + authToken);
	        }
	      }
	      filterChain.doFilter(servletRequest, servletResponse);
	    } catch (NiyopayAuthenticationException | ArrayIndexOutOfBoundsException | UsernameNotFoundException | UserNotActivatedException exception) {
	      //logger.error(exception.getMessage(), exception);
	      HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
	      httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	      //logger.error(ex.getMessage(), ex);
	      HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
	      httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
	    }
	  }
	
}
