package com.niyo.customersupport.security.repositories.mongo;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.niyo.customersupport.security.pojos.mongo.ApiUser;

public interface ApiUserRepository extends MongoRepository<ApiUser, Serializable>{
	
	@Query("{_id : ?0, secret : ?1}")
	Optional<ApiUser> getApiUser(String key, String secret);

	@Query("{_id : ?0}")
	Optional<ApiUser> getApiUser(String key);

}
