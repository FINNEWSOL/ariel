package com.niyo.customersupport.security.repositories.mongo;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.niyo.customersupport.security.pojos.mongo.Customer;

public interface CustomerRepository extends MongoRepository<Customer, Serializable> {
	
	public Customer findByCrnAndStatus(String crn, String status);
	
	Optional<Customer> findOneByCrn(String crn);
}
