package com.niyo.customersupport.security.repositories.mongo;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.niyo.customersupport.security.pojos.mongo.Config;

public interface ConfigRepository extends MongoRepository<Config, Serializable>{
	
	

}
