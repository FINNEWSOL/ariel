package com.niyo.customersupport.security.repositories.mongo;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.niyo.customersupport.security.pojos.mongo.PartnerBankMapping;

public interface PartnerBankMappingRepository extends MongoRepository<PartnerBankMapping, Serializable> {
	
	

}
